package com.server.homepoint.model;

import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "shipping_services")
@SQLDelete(sql = "UPDATE shipping_services SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class ShippingServices {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "courier_type", nullable = false)
    private String courierType;

    @Column(name = "icon", nullable = false)
    private String icon;

    @Column(name = "shipping_cost", nullable = false)
    private Integer shippingCost;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;
}
