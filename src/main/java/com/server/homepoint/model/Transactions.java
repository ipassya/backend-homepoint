package com.server.homepoint.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "transactions")
public class Transactions {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "total_price", nullable = false)
    private Double totalPrice;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "payment_proof")
    private String paymentProof;

    @Column(name = "payment_deadline", nullable = false)
    private Date paymentDeadline;

    @Column(name = "store_location", nullable = false)
    private String storeLocation;

    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "addresses_id", referencedColumnName = "id")
    private Addresses addresses;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "bank_id", referencedColumnName = "id")
    private Bank bank;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "shipping_service_id", referencedColumnName = "id")
    private ShippingServices shippingServices;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users users;

    @OneToMany(mappedBy = "transactions", cascade = CascadeType.ALL)
    private List<TransactionItems> transactionItems;
}
