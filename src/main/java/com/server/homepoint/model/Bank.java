package com.server.homepoint.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "bank")
@SQLDelete(sql = "UPDATE bank SET is_deleted = true WHERE id = ?")
@Where(clause = "is_deleted = false")
public class Bank {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "bank_name", nullable = false)
    private String bankName;

    @Column(name = "bank_logo", nullable = false)
    private String bankLogo;

    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @Column(name = "holder_name", nullable = false)
    private String holderName;

    @Column(name = "is_deleted", nullable = false)
    private Boolean isDeleted = false;
}
