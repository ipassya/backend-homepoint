package com.server.homepoint.util;

import javax.servlet.http.HttpServletRequest;

public class Urlutil {

    public static String getSiteURL(HttpServletRequest request){
        String siteURL = request.getRequestURI().toString();

        return siteURL.replace(request.getServletPath(),"");
    }
}
