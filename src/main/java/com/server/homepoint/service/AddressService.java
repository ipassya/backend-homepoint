package com.server.homepoint.service;

import com.server.homepoint.dto.address.AddressDto;
import com.server.homepoint.dto.address.AddressInsertDto;
import com.server.homepoint.dto.address.AddressReadDto;
import com.server.homepoint.dto.address.AddressUpdateDto;
import com.server.homepoint.model.Addresses;

import java.util.List;
import java.util.UUID;

public interface AddressService {

    public AddressDto create(AddressInsertDto addressInsertDto);

    public AddressDto update(AddressUpdateDto addressUpdateDto);

    public List<AddressDto> getall();

    public Addresses findOne(UUID id);

    public void removeOne(UUID id);
}
