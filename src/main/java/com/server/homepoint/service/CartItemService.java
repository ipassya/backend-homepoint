package com.server.homepoint.service;

import com.server.homepoint.dto.cartItem.CartItemBulkDeleteDto;
import com.server.homepoint.dto.cartItem.CartItemDto;

import java.util.List;
import java.util.UUID;

public interface CartItemService {
    List<CartItemDto> getCartItems(UUID userId);
    CartItemDto findProduct(UUID userId, UUID productId);
    CartItemDto addCartItem(UUID userId, UUID productId, Integer quantity);
    CartItemDto updateCartItem(UUID id, Integer quantity);
    CartItemDto deleteCartItem(UUID id);
    List<CartItemDto> deleteBulkCartItems(CartItemBulkDeleteDto ids);
}
