package com.server.homepoint.service;

import com.server.homepoint.dto.user.*;
import com.server.homepoint.model.Users;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;

public interface UserService {

    public UserDto create(RegisterDto registerDto) throws Exception;

    public List<UserDto> getAll();

    public UserDto getByEmail(String email) throws Exception;

    public UserDto getById(UUID id) throws Exception;

    public UserDto update(UserUpdateDto userDto) throws Exception;

    public UserDto updatePassword(UpdatePasswordDto updatePasswordDto) throws Exception;

    public void remove(UUID id) throws Exception;

    public void sendVerificationEmail(RegisterDto registerDto, String siteURL) throws MessagingException, UnsupportedEncodingException;

    public boolean verify(String verificationCode);

    public void updateResetPassword(String token, String email) throws Exception;

    public Users getByResetPasswordToken(String resetPasswordToken) throws Exception;

    public void updatePassword(Users users, String newPassword);

    public void sendEmail(String email, String resetPasswordLink) throws MessagingException, UnsupportedEncodingException;

}
