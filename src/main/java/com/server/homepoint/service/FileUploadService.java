package com.server.homepoint.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileUploadService {
    String upload(MultipartFile file) throws IOException;
    List<String> uploadMultiple(MultipartFile[] files) throws IOException;
    Boolean deleteImage(String imgUrl) throws IOException;
}
