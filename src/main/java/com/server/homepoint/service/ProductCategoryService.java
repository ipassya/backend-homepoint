package com.server.homepoint.service;

import com.server.homepoint.dto.productCategory.ProductCategoryDto;

import java.util.List;
import java.util.UUID;

public interface ProductCategoryService {
    List<ProductCategoryDto> findAll();
    ProductCategoryDto findById(UUID id);
    ProductCategoryDto create(ProductCategoryDto productCategoryDto);
    ProductCategoryDto update(UUID id, ProductCategoryDto productCategoryDto);
    ProductCategoryDto delete(UUID id);
}
