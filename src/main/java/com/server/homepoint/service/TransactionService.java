package com.server.homepoint.service;

import com.server.homepoint.dto.Transactions.TransactionReadDto;
import com.server.homepoint.dto.Transactions.TransactionsDto;
import com.server.homepoint.dto.Transactions.TransactionsInsertDto;
import com.server.homepoint.dto.productCategory.ProductCategoryDto;
import com.server.homepoint.model.Transactions;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface TransactionService {
    List<TransactionReadDto> findByUserId(UUID userId);
    TransactionReadDto findByTransactionId(UUID transactionId);
    TransactionReadDto create(TransactionsInsertDto transactionsInsertDto);
    TransactionReadDto uploadPaymentProof(UUID id, MultipartFile paymentProof) throws IOException;
    TransactionReadDto updateStatus(UUID id, String status);
}
