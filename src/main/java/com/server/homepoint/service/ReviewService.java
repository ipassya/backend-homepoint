package com.server.homepoint.service;

import com.server.homepoint.dto.review.ReviewDto;
import com.server.homepoint.dto.review.ReviewInsertDto;

import java.util.List;
import java.util.UUID;

public interface ReviewService {
    ReviewDto create(ReviewInsertDto reviewDto);
    List<ReviewDto> getByProductId(UUID productId);
}
