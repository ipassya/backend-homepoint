package com.server.homepoint.service.impl;

import com.server.homepoint.dto.user.JpaUserDetails;
import com.server.homepoint.model.Users;
import com.server.homepoint.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = this.userRepository.findByEmail(username).orElseThrow(()-> new UsernameNotFoundException(username + " is not found"));
        return new JpaUserDetails(users);
    }
}
