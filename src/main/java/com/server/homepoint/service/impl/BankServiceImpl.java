package com.server.homepoint.service.impl;

import com.server.homepoint.model.Bank;
import com.server.homepoint.repository.BankRepository;
import com.server.homepoint.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {

    @Autowired
    BankRepository bankRepository;

    @Override
    public Iterable<Bank> getall() {
        return bankRepository.findAll();
    }
}
