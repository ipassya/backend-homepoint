package com.server.homepoint.service.impl;

import com.server.homepoint.dto.user.RoleDto;
import com.server.homepoint.model.ERoles;
import com.server.homepoint.model.Role;
import com.server.homepoint.repository.RoleRepository;
import com.server.homepoint.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public RoleDto create(RoleDto roleDto) throws Exception {
        Role role = new Role();
        ERoles name = null;

        if(roleDto.getName().equalsIgnoreCase("admin")){
            role.setName(ERoles.ROLE_ADMIN);
            name = ERoles.ROLE_ADMIN;
        }else if(roleDto.getName().equalsIgnoreCase("customer")){
            role.setName(ERoles.ROLE_CUSTOMER);
            name = ERoles.ROLE_CUSTOMER;
        }else{
            throw new Exception(roleDto.getName()+" is only admin and customer");
        }

        Optional<Role> roleOptional = roleRepository.findByName(name);

        if(roleOptional.isPresent()){
            throw new Exception(roleDto.getName() + "Has DUplicate in Database");
        }

        Role roleSaved = roleRepository.save(role);

        RoleDto newRoleDto = new RoleDto();

        newRoleDto.setId(roleSaved.getId());

        newRoleDto.setName(roleSaved.getName().name());

        return newRoleDto;
    }
}
