package com.server.homepoint.service.impl;

import com.server.homepoint.dto.productCategory.ProductCategoryDto;
import com.server.homepoint.model.ProductCategories;
import com.server.homepoint.repository.ProductCategoryRepository;
import com.server.homepoint.service.ProductCategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ProductCategoryDto> findAll() {
        List<ProductCategories> productCategories = productCategoryRepository.findAll();
        List<ProductCategoryDto> productCategoryDtos = new ArrayList<>();
        for (ProductCategories productCategory : productCategories) {
            productCategoryDtos.add(modelMapper.map(productCategory, ProductCategoryDto.class));

        }

        return productCategoryDtos;
    }

    @Override
    public ProductCategoryDto findById(UUID id) {
        ProductCategories productCategory = productCategoryRepository.findById(id).orElse(null);
        assert productCategory != null;
        return modelMapper.map(productCategory, ProductCategoryDto.class);
    }

    @Override
    public ProductCategoryDto create(ProductCategoryDto productCategoryDto) {
        ProductCategories productCategory = modelMapper.map(productCategoryDto, ProductCategories.class);
        productCategory.setIsDeleted(false);
        productCategory = productCategoryRepository.save(productCategory);
        return modelMapper.map(productCategory, ProductCategoryDto.class);
    }

    @Override
    public ProductCategoryDto update(UUID id, ProductCategoryDto productCategoryDto) {
        Optional<ProductCategories> productCategoryOptional = productCategoryRepository.findById(id);
        if (productCategoryOptional.isPresent()) {
            ProductCategories productCategory;
            productCategory = modelMapper.map(productCategoryDto, ProductCategories.class);
            productCategory.setId(id);
            return modelMapper.map(productCategoryRepository.save(productCategory), ProductCategoryDto.class);
        }

        return null;
    }

    @Override
    public ProductCategoryDto delete(UUID id) {
        Optional<ProductCategories> productCategoryOptional = productCategoryRepository.findById(id);
        if (productCategoryOptional.isPresent()) {
            ProductCategories productCategory = productCategoryOptional.get();
            productCategoryRepository.delete(productCategory);
            return modelMapper.map(productCategory, ProductCategoryDto.class);
        }

        return null;
    }

}
