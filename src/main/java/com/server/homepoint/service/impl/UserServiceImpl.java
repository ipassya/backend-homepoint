package com.server.homepoint.service.impl;

import com.server.homepoint.dto.user.*;
import com.server.homepoint.model.*;
import com.server.homepoint.repository.RoleRepository;
import com.server.homepoint.repository.UserRepository;
import com.server.homepoint.service.UserService;
import net.bytebuddy.utility.RandomString;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.*;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public UserDto create(RegisterDto registerDto) throws Exception {

        Date date = new Date();
        ERoles name;
        name = ERoles.ROLE_CUSTOMER;
        Role role = this.roleRepository.findByName(name).orElseThrow(()->new Exception(name.name()+" cannot be found in database"));
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        Users users = modelMapper.map(registerDto, Users.class);

        users.setPassword(bCryptPasswordEncoder.encode(registerDto.getPassword()));
        users.setJoinedSince(date);
        users.setRoles(roles);
        users.setIsActive(false);

        Cart cart = new Cart();
        Wishlist wishlist = new Wishlist();
        cart.setUsers(users);
        wishlist.setUsers(users);
        users.setCart(cart);
        users.setWishlist(wishlist);

        String randomCode = RandomString .make(64);

        users.setVerificationCode(randomCode);

        users = userRepository.save(users);

        return modelMapper.map(users, UserDto.class);
    }

    @Override
    public void sendVerificationEmail(RegisterDto registerDto, String siteURL) throws MessagingException, UnsupportedEncodingException {

        Users users = userRepository.getUsersByEmail(registerDto.getEmail());

        String subject = "Please verify your Registration";

        String sender = "Homepoint team";

        String mailContent = "<p>Dear "+ registerDto.getName() +",</p>";

        mailContent += "<p> Please click the link in below to verify for your registration </p>";

        String verifyURL = siteURL + "/verify?code=" + users.getVerificationCode();
        mailContent += "<h3><a href=\"" + verifyURL + "\">VERIFY</a></h3>";

        mailContent += "<p>Thank you <br> Homepoint Team </p>";

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("homepoint@gmail.com","Homepoint Team");

        helper.setTo(registerDto.getEmail());

        helper.setSubject(subject);

        helper.setText(mailContent, true);

        mailSender.send(message);
    }


    @Override
    public List<UserDto> getAll() {
       List<Users> usersList = this.userRepository.findAll();
       List<UserDto> userDtoList = new ArrayList<>();
       for (Users users : usersList){
           UserDto dto = modelMapper.map(users, UserDto.class);
           userDtoList.add(dto);
       }
           return userDtoList;
    }

    @Override
    public UserDto update(UserUpdateDto userDto) throws Exception {
        Users user = userRepository.findById(userDto.getId()).orElseThrow(()->new Exception("User cannot be found in database"));
        user.setName(userDto.getName());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setEmail(userDto.getEmail());
        user.setBirthDate(userDto.getBirthDate());
        user.setGender(userDto.getGender());

        user = userRepository.save(user);

        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public UserDto updatePassword(UpdatePasswordDto updatePasswordDto) throws Exception {

        Users user = userRepository.findById(updatePasswordDto.getId()).orElseThrow(()->new Exception("User cannot be found in database"));

        user.setPassword(bCryptPasswordEncoder.encode(updatePasswordDto.getPassword()));

        user = userRepository.save(user);

        return modelMapper.map(user, UserDto.class);
    }


    @Override
    public UserDto getByEmail(String email) throws Exception {
        Users users = this.userRepository.findByEmail(email).orElseThrow(() ->
                new Exception(email + " not found"));
        UserDto dto = modelMapper.map(users, UserDto.class);
        return dto;
    }

    @Override
    public UserDto getById(UUID id) throws Exception {
        Users users =  this.userRepository.findById(id).orElseThrow(()->
                new Exception(id + "Tidak ditemukan"));
        UserDto dto = modelMapper.map(users, UserDto.class);

        return dto;
    }

    @Override
    public void remove(UUID id) throws Exception {
        userRepository.deleteById(id);
    }

    @Override
    public boolean verify(String verificationCode) {
        Users users = userRepository.findByVerificationCode(verificationCode);

        if(users == null || users.getIsActive() == true) {
            return false;
        }else{
            userRepository.enable(users.getId());

            return true;
        }
    }

    @Override
    public void updateResetPassword(String token, String email) throws Exception {
        Users users = userRepository.getUsersByEmail(email);

        if(users != null){
            users.setResetPasswordToken(token);
            userRepository.save(users);
        }else{
            throw new Exception("Email " + email + " tidak ditemukan");
        }
    }

    @Override
    public Users getByResetPasswordToken(String resetPasswordToken) throws Exception {
        Users users = userRepository.findByResetPasswordToken(resetPasswordToken);

        if(users != null){
            return users;
        }else{
            throw new Exception("Permintaan reset password " + users + " tidak ditemukan");
        }
    }

    @Override
    public void updatePassword(Users users, String newPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String encodePassword = passwordEncoder.encode(newPassword);

        users.setPassword(encodePassword);
        users.setResetPasswordToken(null);

        userRepository.save(users);
    }

    @Override
    public void sendEmail(String email, String resetPasswordLink) throws MessagingException, UnsupportedEncodingException {
        Users users = userRepository.getUsersByEmail(email);

        String subject = "Here's the link to reset your password";

        String sender = "Homepoint team";

        String mailContent = "<p>Dear "+ users.getName() +",</p>";

        mailContent += "<p> Please click the link in below to reset for your Password </p>";

        mailContent += "<h3><a href=\"" + resetPasswordLink + "\">Reset Password</a></h3>";

        mailContent += "<p>Thank you <br> Homepoint Team </p>";

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom("homepoint@gmail.com","Homepoint Team");

        helper.setTo(users.getEmail());

        helper.setSubject(subject);

        helper.setText(mailContent, true);

        mailSender.send(message);

    }
}
