package com.server.homepoint.service.impl;

import com.server.homepoint.dto.Transactions.TransactionReadDto;
import com.server.homepoint.dto.Transactions.TransactionsDto;
import com.server.homepoint.dto.Transactions.TransactionsInsertDto;
import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.transactionItem.TransactionItemDto;
import com.server.homepoint.dto.transactionItem.TransactionItemInsertDto;
import com.server.homepoint.model.Products;
import com.server.homepoint.model.TransactionItems;
import com.server.homepoint.model.Transactions;
import com.server.homepoint.model.Users;
import com.server.homepoint.repository.*;
import com.server.homepoint.service.TransactionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Transactional
public class TransactionsServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    TransactionItemRespository transactionItemRespository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    BankRepository bankRepository;

    @Autowired
    ShippingServicesRepository shippingServicesRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    FileUploadServiceImpl fileUploadService;

    @Override
    public List<TransactionReadDto> findByUserId(UUID userId) {
        List<Transactions> transactions = transactionRepository.findByUserIdAndSortByDateDesc(userId);
        List<TransactionReadDto> transactionsDtos = new ArrayList<>();
        for (Transactions transaction : transactions) {
            transactionsDtos.add(modelMapper.map(transaction, TransactionReadDto.class));
        }

        return transactionsDtos;
    }

    @Override
    public TransactionReadDto findByTransactionId(UUID transactionId) {
        Transactions transactions = transactionRepository.findById(transactionId).get();
        return modelMapper.map(transactions, TransactionReadDto.class);
    }

    @Override
    public TransactionReadDto create(TransactionsInsertDto transactionsInsertDto) {
        Transactions transactions = modelMapper.map(transactionsInsertDto, Transactions.class);

        transactions.setStatus("Belum Dibayar");
        transactions.setCreatedAt(new Date());
        transactions.setPaymentDeadline(new Date(new Date().getTime() + 86400000));


        transactions.setUsers(userRepository.findById(transactionsInsertDto.getUserId()).get());

        transactions.setBank(bankRepository.findById(transactionsInsertDto.getBankId()).get());

        transactions.setShippingServices(shippingServicesRepository.findById(transactionsInsertDto.getShippingServicesId()).get());

        transactions.setAddresses(addressRepository.findById(transactionsInsertDto.getAddressesId()).get());

        List<TransactionItems> transactionItems = new ArrayList<>();
        for(TransactionItemInsertDto transactionItemDto : transactionsInsertDto.getTransactionItems()){
            TransactionItems transactionItem = modelMapper.map(transactionItemDto, TransactionItems.class);
            transactionItem.setProducts(productRepository.findById(transactionItemDto.getProductId()).get());
            transactionItem.setTransactions(transactions);
            if (Boolean.TRUE.equals(transactionItemDto.getIsInsurance())){
                int insuranceFee = (transactionItemDto.getPrice() * (100 - transactionItemDto.getDiscount()) / 100) / 100;
                transactionItem.setInsuranceFee(insuranceFee);
            }

            transactionItems.add(transactionItem);
        }

        transactions.setTransactionItems(transactionItems);

        transactions = transactionRepository.save(transactions);

        return modelMapper.map(transactions, TransactionReadDto.class);
    }

    @Override
    public TransactionReadDto uploadPaymentProof(UUID id, MultipartFile paymentProof) throws IOException {
        Transactions transaction = transactionRepository.findById(id).get();
        String paymentProofUrl = fileUploadService.upload(paymentProof);

        transaction.setPaymentProof(paymentProofUrl);
        transaction.setStatus("Menunggu Konfirmasi");

        transaction = transactionRepository.save(transaction);
        return modelMapper.map(transaction, TransactionReadDto.class);
    }

    @Override
    public TransactionReadDto updateStatus(UUID id, String status) {
        Transactions transaction = transactionRepository.findById(id).get();
        transaction.setStatus(status);
        List<Products> products = new ArrayList<>();
        if (status.equals("Dikemas")){
            for (TransactionItems transactionItem : transaction.getTransactionItems()){
                Products product = productRepository.findById(transactionItem.getProducts().getId()).get();
                product.setStock(product.getStock() - transactionItem.getQuantity());
                products.add(product);
            }
            productRepository.saveAll(products);
        }
        return modelMapper.map(transactionRepository.save(transaction), TransactionReadDto.class);
    }
}
