package com.server.homepoint.service.impl;

import com.server.homepoint.dto.wishlistItem.WishlistItemBulkDeleteDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemPaginationDto;
import com.server.homepoint.model.Cart;
import com.server.homepoint.model.CartItems;
import com.server.homepoint.model.Wishlist;
import com.server.homepoint.model.WishlistItems;
import com.server.homepoint.repository.*;
import com.server.homepoint.service.WishlistItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class WishlistItemServiceImpl implements WishlistItemService {

    @Autowired
    private WishlistRepository wishlistRepository;

    @Autowired
    private WishlistItemRepository wishlistItemRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public WishlistItemPaginationDto getWishlistItems(UUID userId, Integer page, Integer size, String productName) {
        Page<WishlistItems> wishlistItems = null;
        Pageable paging = PageRequest.of(page, size);
        productName = productName == null ? productName : productName.toLowerCase();

        if (productName == null) {
            wishlistItems = wishlistItemRepository.findByUserIdAndSortByDateDesc(userId, paging);
        } else {
            wishlistItems = wishlistItemRepository.findByUserIdAndProductNameAndSortByDateDesc(userId, productName, paging);
        }

        int totalPages = wishlistItems.getTotalPages();
        long totalElements = wishlistItems.getTotalElements();
        int currentPage = wishlistItems.getNumber();
        int sizeOfPage = wishlistItems.getContent().size();

        List<WishlistItemDto> wishlistItemDtos = wishlistItems.stream()
                .map(wishlistItem -> modelMapper.map(wishlistItem, WishlistItemDto.class))
                .collect(Collectors.toList());

        return new WishlistItemPaginationDto()
                .setTotalPage(totalPages)
                .setTotalRecord(totalElements)
                .setCurrentPage(currentPage)
                .setPageSize(sizeOfPage)
                .setWishlistItems(wishlistItemDtos);
    }

    @Override
    public WishlistItemDto findProduct(UUID userId, UUID productId) {
        Wishlist wishlist = wishlistRepository.findByUserId(userId).get();
        WishlistItems wishlistItem = wishlistItemRepository.findProduct(wishlist.getId(), productId);
        if (wishlistItem == null) {
            return null;
        }

        return modelMapper.map(wishlistItem, WishlistItemDto.class);
    }

    @Override
    public WishlistItemDto addWishlistItem(UUID userId, UUID productId) {
        Wishlist wishlist = wishlistRepository.findByUserId(userId).get();
        WishlistItems wishlistItem = wishlistItemRepository.findProduct(wishlist.getId(), productId);

        Cart cart = cartRepository.findByUserId(userId).get();
        CartItems cartItem = cartItemRepository.findProduct(cart.getId(), productId);
        if (cartItem != null) {
            cartItemRepository.delete(cartItem);
        }

        if (wishlistItem == null) {
            wishlistItem = new WishlistItems();
            wishlistItem.setWishlist(wishlist);
            wishlistItem.setProducts(productRepository.findById(productId).get());
            wishlistItem.setCreatedAt(new Date());
            wishlistItemRepository.save(wishlistItem);
        }

        return modelMapper.map(wishlistItem, WishlistItemDto.class);
    }

    @Override
    public WishlistItemDto deleteWishlistItem(UUID id) {
        Optional<WishlistItems> wishlistItemOptional = wishlistItemRepository.findById(id);
        if (wishlistItemOptional.isPresent()) {
            WishlistItems wishlistItem = modelMapper.map(wishlistItemOptional.get(), WishlistItems.class);
            wishlistItemRepository.delete(wishlistItem);

            return modelMapper.map(wishlistItem, WishlistItemDto.class);
        }

        return null;
    }

    @Override
    public List<WishlistItemDto> deleteBulkWishlistItems(WishlistItemBulkDeleteDto ids) {
        List<WishlistItems> wishlistItems = wishlistItemRepository.findAllById(Arrays.asList(ids.getWishlistItemIds()));
        wishlistItemRepository.deleteAll(wishlistItems);
        return wishlistItems.stream()
                .map(wishlistItem -> modelMapper.map(wishlistItem, WishlistItemDto.class))
                .collect(Collectors.toList());
    }
}

