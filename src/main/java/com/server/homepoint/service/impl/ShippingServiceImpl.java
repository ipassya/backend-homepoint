package com.server.homepoint.service.impl;

import com.server.homepoint.model.ShippingServices;
import com.server.homepoint.repository.ShippingServicesRepository;
import com.server.homepoint.service.ShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShippingServiceImpl implements ShippingService {

    @Autowired
    ShippingServicesRepository shippingServicesRepository;

    @Override
    public Iterable<ShippingServices> getall() {
        return shippingServicesRepository.findAll();
    }
}
