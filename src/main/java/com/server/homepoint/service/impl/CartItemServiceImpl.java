package com.server.homepoint.service.impl;

import com.server.homepoint.dto.cartItem.CartItemBulkDeleteDto;
import com.server.homepoint.dto.cartItem.CartItemDto;
import com.server.homepoint.model.Cart;
import com.server.homepoint.model.CartItems;
import com.server.homepoint.model.Wishlist;
import com.server.homepoint.model.WishlistItems;
import com.server.homepoint.repository.*;
import com.server.homepoint.service.CartItemService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CartItemServiceImpl implements CartItemService {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private WishlistRepository wishlistRepository;

    @Autowired
    private WishlistItemRepository wishlistItemRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CartItemDto> getCartItems(UUID userId) {
        List<CartItems> cartItems = cartItemRepository.findByUserIdAndSortByDateDesc(userId);
        List<CartItemDto> cartItemDtos = new ArrayList<>();
        for (CartItems cartItem : cartItems) {
            cartItemDtos.add(modelMapper.map(cartItem, CartItemDto.class));
        }

        return cartItemDtos;
    }

    @Override
    public CartItemDto findProduct(UUID userId, UUID productId) {
        Cart cart = cartRepository.findByUserId(userId).get();
        CartItems cartItem = cartItemRepository.findProduct(cart.getId(), productId);
        if (cartItem == null) {
            return null;
        }

        return modelMapper.map(cartItem, CartItemDto.class);
    }

    @Override
    public CartItemDto addCartItem(UUID userId, UUID productId, Integer quantity) {
        Cart cart = cartRepository.findByUserId(userId).get();
        CartItems cartItem = cartItemRepository.findProduct(cart.getId(), productId);
        quantity = (quantity == null || quantity == 0) ? 1 : quantity;

        Wishlist wishlist = wishlistRepository.findByUserId(userId).get();
        WishlistItems wishlistItem = wishlistItemRepository.findProduct(wishlist.getId(), productId);
        if (wishlistItem != null) {
            wishlistItemRepository.delete(wishlistItem);
        }

        if (cartItem == null) {
            cartItem = new CartItems();
            cartItem.setQuantity(quantity);
            cartItem.setCart(cart);
            cartItem.setProducts(productRepository.findById(productId).get());
            cartItem.setUpdatedAt(new Date());
            cartItemRepository.save(cartItem);
        } else {
            cartItem.setQuantity(cartItem.getQuantity() + quantity);
            cartItemRepository.save(cartItem);
        }

        return modelMapper.map(cartItem, CartItemDto.class);
    }

    @Override
    public CartItemDto updateCartItem(UUID id, Integer quantity) {
        Optional<CartItems> cartItemOptional = cartItemRepository.findById(id);
        if (cartItemOptional.isPresent()) {
            CartItems cartItem;
            cartItem = modelMapper.map(cartItemOptional.get(), CartItems.class);
            cartItem.setQuantity(quantity);
            cartItem.setUpdatedAt(new Date());
            cartItemRepository.save(cartItem);

            return modelMapper.map(cartItem, CartItemDto.class);
        }

        return null;
    }

    @Override
    public CartItemDto deleteCartItem(UUID id) {
        Optional<CartItems> cartItemOptional = cartItemRepository.findById(id);
        if (cartItemOptional.isPresent()) {
            CartItems cartItem;
            cartItem = modelMapper.map(cartItemOptional.get(), CartItems.class);
            cartItemRepository.delete(cartItem);

            return modelMapper.map(cartItem, CartItemDto.class);
        }

        return null;
    }

    @Override
    public List<CartItemDto> deleteBulkCartItems(CartItemBulkDeleteDto ids) {
        List<CartItems> cartItems = cartItemRepository.findAllById(Arrays.asList(ids.getCartItemIds()));
        cartItemRepository.deleteAll(cartItems);
        return cartItems.stream()
                .map(cartItem -> modelMapper.map(cartItem, CartItemDto.class))
                .collect(Collectors.toList());
    }
}
