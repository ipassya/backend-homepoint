package com.server.homepoint.service.impl;

import com.server.homepoint.dto.review.ReviewDto;
import com.server.homepoint.dto.review.ReviewInsertDto;
import com.server.homepoint.model.Products;
import com.server.homepoint.model.Reviews;
import com.server.homepoint.model.TransactionItems;
import com.server.homepoint.repository.ProductRepository;
import com.server.homepoint.repository.ReviewRepository;
import com.server.homepoint.repository.TransactionItemRespository;
import com.server.homepoint.repository.UserRepository;
import com.server.homepoint.service.ReviewService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TransactionItemRespository transactionItemRespository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ReviewDto create(ReviewInsertDto reviewDto) {
        Reviews review = new Reviews();
        TransactionItems transactionItem = transactionItemRespository.findById(reviewDto.getTransactionItemId()).get();
        Products product = productRepository.findById(transactionItem.getProducts().getId()).get();
        review.setUsers(userRepository.findById(reviewDto.getUserId()).get());
        review.setProducts(product);
        review.setRating(reviewDto.getRating());
        review.setCreateAt(new Date());
        Reviews savedReview = reviewRepository.save(review);

        int totalRating = product.getRatingCount() + 1;
        Double ratingAverage = ((product.getRatingCount() * product.getRatingAverage()) + reviewDto.getRating()) / totalRating;
        product.setRatingCount(totalRating);
        product.setRatingAverage(ratingAverage);
        productRepository.save(product);

        transactionItem.setReviews(savedReview);
        transactionItemRespository.save(transactionItem);

        return modelMapper.map(savedReview, ReviewDto.class);
    }

    @Override
    public List<ReviewDto> getByProductId(UUID productId) {
        List<Reviews> reviews = reviewRepository.findByProductId(productId);
        List<ReviewDto> reviewDtos = new ArrayList<>();
        for (Reviews review : reviews) {
            reviewDtos.add(modelMapper.map(review, ReviewDto.class));
        }
        return reviewDtos;
    }
}
