package com.server.homepoint.service.impl;

import com.server.homepoint.dto.address.AddressDto;
import com.server.homepoint.dto.address.AddressInsertDto;
import com.server.homepoint.dto.address.AddressReadDto;
import com.server.homepoint.dto.address.AddressUpdateDto;
import com.server.homepoint.model.Addresses;
import com.server.homepoint.repository.AddressRepository;
import com.server.homepoint.repository.UserRepository;
import com.server.homepoint.service.AddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    ModelMapper modelMapper;


    @Override
    public AddressDto create(AddressInsertDto addressInsertDto) {

        Addresses addresses = modelMapper.map(addressInsertDto, Addresses.class);

        addresses.setUsers(userRepository.findById(addressInsertDto.getUsersId()).get());

        addresses = addressRepository.save(addresses);

        return modelMapper.map(addresses, AddressDto.class);
    }

    @Override
    public AddressDto update(AddressUpdateDto addressUpdateDto) {

        Addresses addresses = modelMapper.map(addressUpdateDto, Addresses.class);

        addresses.setUsers(userRepository.findById(addressUpdateDto.getUsersId()).get());

        addresses = addressRepository.save(addresses);

        return modelMapper.map(addresses, AddressDto.class);
    }

    @Override
    public List<AddressDto> getall() {

        List<Addresses> addressDtos = addressRepository.findAll();

        List<AddressDto> addressDtoList = new ArrayList<>();

        for (Addresses addresses : addressDtos){
            AddressDto dto = modelMapper.map(addresses, AddressDto.class);

            addressDtoList.add(dto);
        }

        return addressDtoList;
    }

    @Override
    public Addresses findOne(UUID id) {
        Optional<Addresses> addresses = addressRepository.findById(id);

        if(!addresses.isPresent()){
            return null;
        }

        return addresses.get();
    }

    @Override
    public void removeOne(UUID id) {
        addressRepository.deleteById(id);
    }


}
