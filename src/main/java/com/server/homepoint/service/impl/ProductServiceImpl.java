package com.server.homepoint.service.impl;

import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.product.ProductPaginationDto;
import com.server.homepoint.model.ProductImages;
import com.server.homepoint.model.Products;
import com.server.homepoint.repository.ProductImageRepository;
import com.server.homepoint.repository.ProductRepository;
import com.server.homepoint.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductImageRepository productImageRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ProductPaginationDto findProducts(
            Integer page, Integer size, String name, UUID subcategoryId, Double priceMin, Double priceMax, Double rating, String brand,
            String color, String description, Boolean sortByPriceAsc, Boolean sortByPriceDesc, Boolean sortByBestSeller, Boolean sortByLatest, Boolean sortByBiggestDiscount
    ) {
        Page<Products> products = null;
        name = name == null ? name : name.toLowerCase();
        brand = brand == null ? brand : brand.toLowerCase();
        color = color == null ? color : color.toLowerCase();
        description = description == null ? description : description.toLowerCase();

        products = searchProducts(
                page, size, name, subcategoryId, priceMin, priceMax, rating, brand, color, description, sortByPriceAsc,
                sortByPriceDesc, sortByBestSeller, sortByLatest, sortByBiggestDiscount
        );

        int totalPages = products.getTotalPages();
        long totalElements = products.getTotalElements();
        int currentPage = products.getNumber();
        int sizeOfPage = products.getContent().size();

        List<String> brands = new ArrayList<>();
        List<String> colors = new ArrayList<>();
        Page<Products> productsPage = null;
        for (int i = 0; i < totalPages; i++) {
            productsPage = searchProducts(
                    i, size, name, subcategoryId, priceMin, priceMax, rating, brand, color, description, sortByPriceAsc,
                    sortByPriceDesc, sortByBestSeller, sortByLatest, sortByBiggestDiscount
            );

            brands.addAll(productsPage.stream()
                    .map(Products::getBrand)
                    .distinct()
                    .collect(Collectors.toList()));

            colors.addAll(productsPage.stream()
                    .map(Products::getColor)
                    .distinct()
                    .collect(Collectors.toList()));
        }

        brands = brands.stream()
                .distinct()
                .collect(Collectors.toList());

        colors = colors.stream()
                .distinct()
                .collect(Collectors.toList());

        List<ProductDto> productDtos = products.stream()
                .map(product -> modelMapper.map(product, ProductDto.class))
                .collect(Collectors.toList());

        return new ProductPaginationDto()
                .setTotalPage(totalPages)
                .setTotalRecord(totalElements)
                .setCurrentPage(currentPage)
                .setPageSize(sizeOfPage)
                .setProducts(productDtos)
                .setBrands(brands)
                .setColors(colors);
    }

    @Override
    public ProductDto findById(UUID id) {
        Products product = productRepository.findById(id).orElse(null);
        assert product != null;
        return modelMapper.map(product, ProductDto.class);
    }

    @Override
    public List<ProductDto> findLatestProducts() {
        List<Products> products = productRepository.findLatestProducts();
        List<ProductDto> productDtos = new ArrayList<>();
        for (Products product : products) {
            productDtos.add(modelMapper.map(product, ProductDto.class));
        }

        return productDtos;
    }

    @Override
    public List<ProductDto> findDiscountProducts() {
        List<Products> products = productRepository.findDiscountProducts();
        List<ProductDto> productDtos = new ArrayList<>();
        for (Products product : products) {
            productDtos.add(modelMapper.map(product, ProductDto.class));
        }

        return productDtos;
    }

    @Override
    public ProductDto create(ProductDto productDto) {
        Products product = modelMapper.map(productDto, Products.class);
        product.setIsDeleted(false);
        Set<ProductImages> productImages = product.getProductImages();
        product = productRepository.save(product);

        Products productId = new Products();
        productId.setId(product.getId());
        for (ProductImages productImage : productImages) {
            productImage.setProducts(productId);
        }
        productImageRepository.saveAll(productImages);

        return modelMapper.map(product, ProductDto.class);
    }

    @Override
    public ProductDto update(UUID id, ProductDto productDto) {
        Optional<Products> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            Products product;
            product = modelMapper.map(productDto, Products.class);
            product.setId(id);
            return modelMapper.map(productRepository.save(product), ProductDto.class);
        }

        return null;
    }

    @Override
    public ProductDto delete(UUID id) {
        Optional<Products> productOptional = productRepository.findById(id);
        if (productOptional.isPresent()) {
            Products product = productOptional.get();
            productRepository.delete(product);
            return modelMapper.map(product, ProductDto.class);
        }

        return null;
    }

    // method for search products
    private Page<Products> searchProducts(
            Integer page, Integer size, String name, UUID subcategoryId, Double priceMin, Double priceMax, Double rating, String brand, String color,
            String description, Boolean sortByPriceAsc, Boolean sortByPriceDesc, Boolean sortByBestSeller, Boolean sortByLatest, Boolean sortByBiggestDiscount
    ) {
        Pageable paging = PageRequest.of(page, size);
        double ratingDefault = rating == null ? 0 : rating;
        Page<Products> products = null;

        if (subcategoryId != null) {
            // findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
            if (priceMin != null && priceMax != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                }
            }
            // findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
            else if (priceMin != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(
                            name, priceMin, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                }
            }
            // findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
            else if (priceMax != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(
                            name, priceMax, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                }
            }
            // findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
            else if (name != null || rating != null || brand != null || color != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                } else {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(
                            name, ratingDefault, brand, color, description, subcategoryId, paging
                    );
                }
            } else {
                products = productRepository.findAll(paging);
            }
        } else {
            // findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescription
            if (priceMin != null && priceMax != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescription(
                            name, priceMin, priceMax, ratingDefault, brand, color, description, paging
                    );
                }
            }
            // findByNameAndPriceMinAndRatingAndBrandAndColorAndDescription
            else if (priceMin != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceMinAndRatingAndBrandAndColorAndDescription(
                            name, priceMin, ratingDefault, brand, color, description, paging
                    );
                }
            }
            // findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescription
            else if (priceMax != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                } else {
                    products = productRepository.findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescription(
                            name, priceMax, ratingDefault, brand, color, description, paging
                    );
                }
            }
            // findByNameAndRatingAndBrandAndColorAndDescription
            else if (name != null || rating != null || brand != null || color != null) {
                if (Boolean.TRUE.equals(sortByPriceAsc)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(
                            name, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByPriceDesc)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(
                            name, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBestSeller)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(
                            name, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByLatest)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(
                            name, ratingDefault, brand, color, description, paging
                    );
                } else if (Boolean.TRUE.equals(sortByBiggestDiscount)) {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(
                            name, ratingDefault, brand, color, description, paging
                    );
                } else {
                    products = productRepository.findByNameAndRatingAndBrandAndColorAndDescription(
                            name, ratingDefault, brand, color, description, paging
                    );
                }
            } else {
                products = productRepository.findAll(paging);
            }
        }

        return products;
    }
}
