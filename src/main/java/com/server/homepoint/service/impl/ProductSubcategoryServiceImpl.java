package com.server.homepoint.service.impl;

import com.server.homepoint.dto.productSubcategory.ProductSubcategoryDto;
import com.server.homepoint.dto.productSubcategory.ProductSubcategoryListDto;
import com.server.homepoint.model.ProductSubcategories;
import com.server.homepoint.repository.ProductSubcategoryRepository;
import com.server.homepoint.service.ProductSubcategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductSubcategoryServiceImpl implements ProductSubcategoryService {

    @Autowired
    private ProductSubcategoryRepository productSubcategoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ProductSubcategoryListDto> findAll() {
        List<ProductSubcategories> productSubcategories = productSubcategoryRepository.findAll();
        List<ProductSubcategoryListDto> productSubcategoryListDtos = new ArrayList<>();
        System.out.println("productSubcategories: ");
        for (ProductSubcategories productSubcategory : productSubcategories) {
            productSubcategoryListDtos.add(modelMapper.map(productSubcategory, ProductSubcategoryListDto.class));
//            productSubcategoryDtos.add(new ProductSubcategoryDto(productSubcategory.getId(), productSubcategory.getName(), productSubcategory.getIcon(), productSubcategory.getIsDeleted()));
        }
        System.out.println("productSubcategoryDtos: ");

        return productSubcategoryListDtos;
    }

    @Override
    public ProductSubcategoryDto findById(UUID id) {
        ProductSubcategories productSubcategory = productSubcategoryRepository.findById(id).orElse(null);
        assert productSubcategory != null;
        return modelMapper.map(productSubcategory, ProductSubcategoryDto.class);
    }

    @Override
    public ProductSubcategoryDto create(ProductSubcategoryDto productSubcategoryDto) {
        ProductSubcategories productSubcategory = modelMapper.map(productSubcategoryDto, ProductSubcategories.class);
        productSubcategory.setIsDeleted(false);
        productSubcategory = productSubcategoryRepository.save(productSubcategory);
        return modelMapper.map(productSubcategory, ProductSubcategoryDto.class);
    }

    @Override
    public ProductSubcategoryDto update(UUID id, ProductSubcategoryDto productSubcategoryDto) {
        Optional<ProductSubcategories> productSubcategoryOptional = productSubcategoryRepository.findById(id);
        if (productSubcategoryOptional.isPresent()) {
            ProductSubcategories productSubcategory;
            productSubcategory = modelMapper.map(productSubcategoryDto, ProductSubcategories.class);
            productSubcategory.setId(id);
            return modelMapper.map(productSubcategoryRepository.save(productSubcategory), ProductSubcategoryDto.class);
        }

        return null;
    }

    @Override
    public ProductSubcategoryDto delete(UUID id) {
        Optional<ProductSubcategories> productSubcategoryOptional = productSubcategoryRepository.findById(id);
        if (productSubcategoryOptional.isPresent()) {
            ProductSubcategories productSubcategory = productSubcategoryOptional.get();
            productSubcategoryRepository.delete(productSubcategory);
            return modelMapper.map(productSubcategory, ProductSubcategoryDto.class);
        }

        return null;
    }
}
