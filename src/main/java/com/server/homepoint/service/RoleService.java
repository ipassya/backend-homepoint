package com.server.homepoint.service;

import com.server.homepoint.dto.user.RoleDto;

public interface RoleService {
    RoleDto create(RoleDto roleDto) throws Exception;
}
