package com.server.homepoint.service;

import com.server.homepoint.model.Bank;

public interface BankService {
    public Iterable<Bank> getall();
}
