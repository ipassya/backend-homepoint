package com.server.homepoint.service;

import com.server.homepoint.dto.productSubcategory.ProductSubcategoryDto;
import com.server.homepoint.dto.productSubcategory.ProductSubcategoryListDto;

import java.util.List;
import java.util.UUID;

public interface ProductSubcategoryService {
    List<ProductSubcategoryListDto> findAll();
    ProductSubcategoryDto findById(UUID id);
    ProductSubcategoryDto create(ProductSubcategoryDto productSubcategoryDto);
    ProductSubcategoryDto update(UUID id, ProductSubcategoryDto productSubcategoryDto);
    ProductSubcategoryDto delete(UUID id);
}
