package com.server.homepoint.service;

import com.server.homepoint.dto.wishlistItem.WishlistItemBulkDeleteDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemPaginationDto;

import java.util.List;
import java.util.UUID;

public interface WishlistItemService {
    WishlistItemPaginationDto getWishlistItems(UUID userId, Integer page, Integer size, String productName);
    WishlistItemDto findProduct(UUID userId, UUID productId);
    WishlistItemDto addWishlistItem(UUID userId, UUID productId);
    WishlistItemDto deleteWishlistItem(UUID id);
    List<WishlistItemDto> deleteBulkWishlistItems(WishlistItemBulkDeleteDto ids);
}
