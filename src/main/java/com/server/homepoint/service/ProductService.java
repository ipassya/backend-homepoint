package com.server.homepoint.service;

import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.product.ProductPaginationDto;

import java.util.List;
import java.util.UUID;

public interface ProductService {
    ProductPaginationDto findProducts(Integer page, Integer size, String name, UUID subcategoryId, Double priceMin, Double priceMax, Double rating, String brand,
                                      String color, String description, Boolean sortByPriceAsc, Boolean sortByPriceDesc, Boolean sortByBestSeller, Boolean sortByLatest, Boolean sortByBiggestDiscount
    );
    ProductDto findById(UUID id);
    List<ProductDto> findLatestProducts();
    List<ProductDto> findDiscountProducts();
    ProductDto create(ProductDto productDto);
    ProductDto update(UUID id, ProductDto productDto);
    ProductDto delete(UUID id);
}
