package com.server.homepoint.seeder;

import com.server.homepoint.model.*;
import com.server.homepoint.repository.*;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import com.github.javafaker.Faker;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.persistence.Column;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class DatabaseSeeder {
    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private ProductCategoryRepository productCategoryRepository;
    private ProductSubcategoryRepository productSubcategoryRepository;
    private ProductRepository productRepository;
    private ShippingServicesRepository shippingServicesRepository;
    private BankRepository bankRepository;

    private Faker faker = new Faker(Locale.forLanguageTag("id-ID"));

    @Autowired
    public DatabaseSeeder(
            RoleRepository roleRepository,
            UserRepository userRepository,
            ProductCategoryRepository productCategoryRepository,
            ProductSubcategoryRepository productSubcategoryRepository,
            ProductRepository productRepository,
            ShippingServicesRepository shippingServicesRepository,
            BankRepository bankRepository
    ) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.productSubcategoryRepository = productSubcategoryRepository;
        this.productRepository = productRepository;
        this.shippingServicesRepository = shippingServicesRepository;
        this.bankRepository = bankRepository;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event) throws Exception {
        seedRoles();
        seedProductCategories();
        seedProducts();
        seedBank();
        seedShippingService();
    }

    private void seedRoles() throws Exception {
        if (roleRepository.count() == 0) {
            roleRepository.saveAll(Arrays.asList(
                    new Role(ERoles.ROLE_ADMIN),
                    new Role(ERoles.ROLE_CUSTOMER)
            ));
        }

//        ERoles name;
//
//        name = ERoles.ROLE_CUSTOMER;
//
//        Role role = roleRepository.findByName(name).orElseThrow(()->new Exception( name.name()+" cannot be found in database"));
//
//        Set<Role> roles = new HashSet<>();
//
//        roles.add(role);

//        userRepository.saveAll(Arrays.asList(
//
//
//                 new Users("Administrator",
//                         "admin@gmail.com",
//                         String.valueOf(new BCryptPasswordEncoder(Integer.parseInt("12345"))),
//                         ERoles.ROLE_CUSTOMER)
//        ));
    }

    private void seedBank() {
        if (bankRepository.count() == 0) {
            String[] bankName = {"Bank BTN", "Bank BNI", "Bank Mandiri", "Bank BCA", "Bank BRI"};
            String[] bankLogo = {
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658493842/Homepoint/Bank/Bank_BTN_1_sutskk.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658493842/Homepoint/Bank/Bank_BNI_1_l8mbon.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658493842/Homepoint/Bank/Bank_Mandiri_1_hfubld.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658493842/Homepoint/Bank/Bank_BCA_1_osp0jt.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658493841/Homepoint/Bank/Bank_BRI_1_szzt3w.png"
            };
            String[] accountNumber = {"2546853149672564", "1433997209", "594581325548964", "85265844996", "655801028826532"};
            String[] holderName = {"PT. Hartono B2C", "PT. Hartono B2C", "PT. Hartono B2C", "PT. Hartono B2C", "PT. Hartono B2C"};

            for (int i = 0; i < bankName.length; i++) {
                bankRepository.save(new Bank(UUID.randomUUID(), bankName[i], bankLogo[i], accountNumber[i], holderName[i], false));
            }
        }

    }

    private void seedShippingService() {
        if (shippingServicesRepository.count() == 0) {
            String[] courierType = {"Regular", "Homepoint", "Ambil ditempat"};
            String[] icon = {
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658490843/Homepoint/Kurir/Kurir_Reguler_foh5lu.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658490843/Homepoint/Kurir/Kurir_Homepoint_ymhrzh.png",
                    "https://res.cloudinary.com/masdoni2/image/upload/v1658490843/Homepoint/Kurir/Ambil_di_tempat_losw5u.png"
            };

            Integer[] shippingCost = {25000,0,0};

            for (int i = 0; i < courierType.length; i++) {
                shippingServicesRepository.save(new ShippingServices(UUID.randomUUID(), courierType[i], icon[i],shippingCost[i],false));
            }
        }

    }

    private void seedProductCategories() {
        if (productCategoryRepository.count() == 0) {
            List<Set<ProductSubcategories>> productSubcategories = new ArrayList<>(
                    Arrays.asList(
                            new HashSet<>(
                                    Arrays.asList(
                                            new ProductSubcategories()
                                                    .setName("Peralatan Masak")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166560/Homepoint/Kategori/Sub%20kategori/Peralatan_masak_km46o8.png"),
                                            new ProductSubcategories()
                                                    .setName("Aksesoris Dapur")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166550/Homepoint/Kategori/Sub%20kategori/Aksesoris_dapur_mixgoa.png"),
                                            new ProductSubcategories()
                                                    .setName("Perlengkapan Masak")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166570/Homepoint/Kategori/Sub%20kategori/Perlengkapan_masak_fmjvgg.png")
                                    )
                            ),
                            new HashSet<>(
                                    Arrays.asList(
                                            new ProductSubcategories()
                                                    .setName("Furnitur Interior")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166551/Homepoint/Kategori/Sub%20kategori/Interior_xmzwrt.png"),
                                            new ProductSubcategories()
                                                    .setName("Furnitur Eksterior")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166553/Homepoint/Kategori/Sub%20kategori/Eksterior_uqd7lo.png")
                                    )
                            ),
                            new HashSet<>(
                                    Arrays.asList(
                                            new ProductSubcategories()
                                                    .setName("Kebersihan Rumah")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166542/Homepoint/Kategori/Sub%20kategori/Kbersihan_rumah_p7hwzs.png"),
                                            new ProductSubcategories()
                                                    .setName("Kebersihan Dapur")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166545/Homepoint/Kategori/Sub%20kategori/Kbersihan_dapur_jprk8a.png"),
                                            new ProductSubcategories()
                                                    .setName("Kebersihan Toilet")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166548/Homepoint/Kategori/Sub%20kategori/Kebersihan_toilet_cx9p8z.png")
                                    )
                            ),
                            new HashSet<>(
                                    Arrays.asList(
                                            new ProductSubcategories()
                                                    .setName("Elektronik Dapur")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166564/Homepoint/Kategori/Sub%20kategori/Elektronik_dapur_h7oqpf.png"),
                                            new ProductSubcategories()
                                                    .setName("Elektronik Kebersihan")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166546/Homepoint/Kategori/Sub%20kategori/Elektronik_kebersihan_hzwjvp.png"),
                                            new ProductSubcategories()
                                                    .setName("Perangkat Elektronik")
                                                    .setIcon("https://res.cloudinary.com/hasalreadybeentaken/image/upload/v1656166554/Homepoint/Kategori/Sub%20kategori/Perangkat_Elektronik_yaa1uq.png")
                                    )
                            )
                    )
            );

            String[] categories = {"Peralatan Dapur", "Furnitur", "Peralatan Kebersihan", "Elektronik"};
            List<ProductCategories> productCategories = new ArrayList<>();
            for (int i = 0; i < categories.length; i++) {
                ProductCategories productCategory = new ProductCategories()
                        .setId(UUID.randomUUID())
                        .setName(categories[i])
                        .setProductSubcategories(productSubcategories.get(i))
                        .setIsDeleted(false);
                productCategories.add(productCategory);
            }

            for (ProductCategories productCategory : productCategories) {
                for (ProductSubcategories productSubcategory : productCategory.getProductSubcategories()) {
                    productSubcategory.setProductCategories(productCategory);
                }
            }

            productCategoryRepository.saveAll(productCategories);
        }

    }

    private void seedProducts() throws IOException {
        if (productRepository.count() == 0) {
            seedProductsByCategory("Peralatan Masak");
            seedProductsByCategory("Perlengkapan Masak");
            seedProductsByCategory("Aksesoris Dapur");
            seedProductsByCategory("Kebersihan Rumah");
            seedProductsByCategory("Kebersihan Dapur");
            seedProductsByCategory("Kebersihan Toilet");
            seedProductsByCategory("Furnitur Interior");
            seedProductsByCategory("Furnitur Eksterior");
            seedProductsByCategory("Elektronik Dapur");
            seedProductsByCategory("Elektronik Kebersihan");
            seedProductsByCategory("Perangkat Elektronik");
        }
    }

    private void seedProductsByCategory(String subCategory) throws IOException {
        ProductSubcategories productSubcategory = productSubcategoryRepository.findByName(subCategory);

        String filePath = "src/main/resources/dataset/data-products-homepoint.xls";
        List<Object> productImage1 = readExcelFile(filePath, subCategory, 0);
        List<Object> productImage2 = readExcelFile(filePath, subCategory, 1);
        List<Object> productImage3 = readExcelFile(filePath, subCategory, 2);
        List<Object> name = readExcelFile(filePath, subCategory, 3);
        List<Object> description = readExcelFile(filePath, subCategory, 4);
        List<Object> brand = readExcelFile(filePath, subCategory, 5);
        List<Object> price = readExcelFile(filePath, subCategory, 6);
        List<Object> color = readExcelFile(filePath, subCategory, 7);
        List<Object> discount = readExcelFile(filePath, subCategory, 8);
        List<Object> stock = readExcelFile(filePath, subCategory, 9);
        List<Object> ratingAverage = readExcelFile(filePath, subCategory, 10);
        List<Object> ratingCount = readExcelFile(filePath, subCategory, 11);
        List<Object> amountSold = readExcelFile(filePath, subCategory, 12);
//        List<Object> createdAt = readExcelFile(filePath, subCategory, 13);

        List<Products> products = new ArrayList<>();
        for (int i = 0; i < productImage1.size(); i++) {
            Set<ProductImages> productImages = new HashSet<>();
            productImages.add(new ProductImages().setImage(productImage1.get(i).toString()));
            productImages.add(new ProductImages().setImage(productImage2.get(i).toString()));
            productImages.add(new ProductImages().setImage(productImage3.get(i).toString()));

            Date dateTime = faker.date().past(100, TimeUnit.DAYS);
            Products product = new Products()
                    .setId(UUID.randomUUID())
                    .setName(name.get(i).toString())
                    .setDescription(description.get(i).toString())
                    .setBrand(brand.get(i).toString())
                    .setPrice(Double.parseDouble(price.get(i).toString()))
                    .setDiscount(Double.parseDouble(discount.get(i).toString()))
                    .setStock((int) Math.floor(Double.parseDouble(stock.get(i).toString())))
                    .setColor(color.get(i).toString())
                    .setRatingAverage(Double.parseDouble(ratingAverage.get(i).toString()))
                    .setRatingCount((int) Math.floor(Double.parseDouble(ratingCount.get(i).toString())))
                    .setAmountSold((int) Math.floor(Double.parseDouble(amountSold.get(i).toString())))
                    .setCreatedAt(String.valueOf(dateTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()))
                    .setIsDeleted(false)
                    .setProductSubcategories(productSubcategory)
                    .setProductImages(productImages);

            for (ProductImages productImage : productImages) {
                productImage.setProducts(product);
            }

            products.add(product);
        }
        productRepository.saveAll(products);
    }

    private List<Object> readExcelFile(String filePath, String SheetName, Integer column) throws IOException {
        HSSFWorkbook excelFile = new HSSFWorkbook(new FileInputStream(filePath));
        HSSFSheet excelSheet = excelFile.getSheet(SheetName);
        FormulaEvaluator evaluator = excelFile.getCreationHelper().createFormulaEvaluator();
        List<Object> data = new ArrayList<>();

        for (int i = 1; i < excelSheet.getPhysicalNumberOfRows(); i++) {
            Row row = excelSheet.getRow(i);
            Cell cell = row.getCell(column);
            switch (cell.getCellType()) {
                case STRING:
                    data.add(cell.getStringCellValue());
                    break;
                case NUMERIC:
                    data.add(cell.getNumericCellValue());
                    break;
                case BOOLEAN:
                    data.add(cell.getBooleanCellValue());
                    break;
                case FORMULA:
                    evaluator.evaluateFormulaCell(cell);
                    data.add(cell.getNumericCellValue());
                    break;
                default:
                    break;
            }

        }

        return data;
    }
}
