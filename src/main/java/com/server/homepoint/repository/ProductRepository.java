package com.server.homepoint.repository;

import com.server.homepoint.model.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Products, UUID> {

    @Query(value = "SELECT * FROM products ORDER BY created_at DESC LIMIT 12", nativeQuery = true)
    List<Products> findLatestProducts();

    @Query(value = "SELECT * FROM products WHERE discount IS NOT NULL ORDER BY discount DESC LIMIT 12", nativeQuery = true)
    List<Products> findDiscountProducts();

    // findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescription
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7%", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescription(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, Pageable pageable);
    // End of findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescription


    // findByNameAndPriceMinAndRatingAndBrandAndColorAndDescription
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6%", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescription(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(String name, double priceMin, double rating, String brand, String color, String description, Pageable pageable);
    // End of findByNameAndPriceMinAndRatingAndBrandAndColorAndDescription


    // findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescription
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6%", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescription(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(String name, double priceMax, double rating, String brand, String color, String description, Pageable pageable);
    // End of findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescription


    // findByNameAndRatingAndBrandAndColorAndDescription
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5%", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescription(String name, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByPriceAsc(String name, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByPriceDesc(String name, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByBestSeller(String name, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByLatest(String name, double rating, String brand, String color, String description, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSortByBiggestDiscount(String name, double rating, String brand, String color, String description, Pageable pageable);
    // End of findByNameAndRatingAndBrandAndColorAndDescription


    // findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8 ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8 ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8 ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8 ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price BETWEEN ?2 AND ?3 AND rating_average BETWEEN ?4 AND 5 AND lower(brand) LIKE %?5% AND lower(color) LIKE %?6% AND lower(description) LIKE %?7% AND subcategory_id = ?8 ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(String name, double priceMin, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);
    // End of findByNameAndPriceBetweenAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId


    // findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price >= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(String name, double priceMin, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);
    // End of findByNameAndPriceMinAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId


    // findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND price <= ?2 AND rating_average BETWEEN ?3 AND 5 AND lower(brand) LIKE %?4% AND lower(color) LIKE %?5% AND lower(description) LIKE %?6% AND subcategory_id = ?7 ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(String name, double priceMax, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);
    // End of findByNameAndPriceMaxAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId


    // findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6 ORDER BY price ASC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceAsc(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6 ORDER BY price DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByPriceDesc(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6 ORDER BY amount_sold DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBestSeller(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6 ORDER BY created_at DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByLatest(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);

    @Query(value = "SELECT * FROM products WHERE lower(name) LIKE %?1% AND rating_average BETWEEN ?2 AND 5 AND lower(brand) LIKE %?3% AND lower(color) LIKE %?4% AND lower(description) LIKE %?5% AND subcategory_id = ?6 ORDER BY discount DESC", nativeQuery = true)
    Page<Products> findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryIdAndSortByBiggestDiscount(String name, double rating, String brand, String color, String description, UUID subcategoryId, Pageable pageable);
    // End of findByNameAndRatingAndBrandAndColorAndDescriptionAndSubcategoryId
}
