package com.server.homepoint.repository;

import com.server.homepoint.model.WishlistItems;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface WishlistItemRepository extends JpaRepository<WishlistItems, UUID> {
    @Query(
            value = "SELECT * FROM wishlist_items WHERE wishlist_id = ?1 AND product_id = ?2",
            nativeQuery = true
    )
    WishlistItems findProduct(UUID wishlistId, UUID productId);

    @Query(
            value = "SELECT wishlist_items.id, wishlist_items.wishlist_id, wishlist_items.product_id, wishlist_items.created_at FROM wishlist_items JOIN wishlist ON wishlist.id = wishlist_items.wishlist_id WHERE wishlist.user_id = ?1 ORDER BY wishlist_items.created_at DESC",
            nativeQuery = true
    )
    Page<WishlistItems> findByUserIdAndSortByDateDesc(UUID userId, Pageable pageable);

    @Query(
            value = "SELECT wishlist_items.id, wishlist_items.wishlist_id, wishlist_items.product_id, wishlist_items.created_at FROM wishlist_items JOIN wishlist ON wishlist.id = wishlist_items.wishlist_id JOIN products ON products.id = wishlist_items.product_id WHERE wishlist.user_id = ?1 AND lower(products.name) LIKE %?2% ORDER BY wishlist_items.created_at DESC",
            nativeQuery = true
    )
    Page<WishlistItems> findByUserIdAndProductNameAndSortByDateDesc(UUID userId, String productName, Pageable pageable);
}
