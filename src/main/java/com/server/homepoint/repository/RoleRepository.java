package com.server.homepoint.repository;

import com.server.homepoint.model.ERoles;
import com.server.homepoint.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    public Optional<Role> findByName(ERoles name);
}
