package com.server.homepoint.repository;

import com.server.homepoint.model.CartItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface CartItemRepository extends JpaRepository<CartItems, UUID> {
    @Query(
            value = "SELECT * FROM cart_items WHERE cart_id = ?1 AND product_id = ?2",
            nativeQuery = true
    )
    CartItems findProduct(UUID cartId, UUID productId);

    @Query(
            value = "SELECT cart_items.id, cart_items.quantity, cart_items.cart_id, cart_items.product_id, cart_items.updated_at FROM cart_items JOIN cart ON cart.id = cart_items.cart_id WHERE cart.user_id = ?1 ORDER BY cart_items.updated_at DESC",
            nativeQuery = true
    )
    List<CartItems> findByUserIdAndSortByDateDesc(UUID userId);
}
