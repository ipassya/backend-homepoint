package com.server.homepoint.repository;

import com.server.homepoint.model.CartItems;
import com.server.homepoint.model.TransactionItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionItemRespository extends JpaRepository<TransactionItems, UUID> {

}
