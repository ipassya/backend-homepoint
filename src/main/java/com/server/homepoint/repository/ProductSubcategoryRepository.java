package com.server.homepoint.repository;

import com.server.homepoint.model.ProductSubcategories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductSubcategoryRepository extends JpaRepository<ProductSubcategories, UUID> {
    @Query(value = "SELECT * FROM product_subcategories WHERE name = ?1", nativeQuery = true)
    ProductSubcategories findByName(String name);
}
