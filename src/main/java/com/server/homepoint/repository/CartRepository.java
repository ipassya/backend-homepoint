package com.server.homepoint.repository;

import com.server.homepoint.model.Cart;
import com.server.homepoint.model.CartItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface CartRepository extends JpaRepository<Cart, UUID> {

    @Query(value = "SELECT * FROM cart WHERE user_id = ?1", nativeQuery = true)
    Optional<Cart> findByUserId(UUID userId);
}
