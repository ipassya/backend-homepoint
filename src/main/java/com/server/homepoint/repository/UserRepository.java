package com.server.homepoint.repository;

import com.server.homepoint.dto.user.UserReadDto;
import com.server.homepoint.dto.user.UserUpdatePasswordDto;
import com.server.homepoint.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<Users, UUID> {
    public Optional<Users> findByEmail(String email);

    public List<Users> getUsersById(UUID id);

    @Query("SELECT u FROM Users u WHERE u.email = :email")
    public Users getUsersByEmail(@Param("email") String email);

    @Query("UPDATE Users u SET u.isActive = true WHERE u.id = ?1")
    @Modifying
    public void enable(UUID id);

    @Query("SELECT u FROM Users u WHERE u.verificationCode = ?1")
    public Users findByVerificationCode(String code);

    @Query("SELECT u FROM Users u Where u.resetPasswordToken = ?1")
    public Users findByResetPasswordToken(String token);
}
