package com.server.homepoint.repository;

import com.server.homepoint.model.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface WishlistRepository extends JpaRepository<Wishlist, UUID> {
    @Query(value = "SELECT * FROM wishlist WHERE user_id = ?1", nativeQuery = true)
    Optional<Wishlist> findByUserId(UUID userId);
}
