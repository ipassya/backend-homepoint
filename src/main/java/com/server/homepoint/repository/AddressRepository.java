package com.server.homepoint.repository;

import com.server.homepoint.dto.address.AddressDto;
import com.server.homepoint.model.Addresses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AddressRepository extends JpaRepository<Addresses, UUID> {
}
