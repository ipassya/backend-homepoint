package com.server.homepoint.repository;

import com.server.homepoint.model.Reviews;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReviewRepository extends JpaRepository<Reviews, UUID> {
    @Query(value = "SELECT * FROM reviews WHERE product_id = ?1", nativeQuery = true)
    List<Reviews> findByProductId(UUID productId);
}
