package com.server.homepoint.repository;

import com.server.homepoint.model.ShippingServices;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShippingServicesRepository extends JpaRepository<ShippingServices, UUID> {
}
