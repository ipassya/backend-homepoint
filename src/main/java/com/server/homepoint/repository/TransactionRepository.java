package com.server.homepoint.repository;

import com.server.homepoint.model.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<Transactions, UUID> {
    @Query(value = "SELECT * FROM transactions WHERE user_id = ?1 ORDER BY created_at DESC", nativeQuery = true)
    List<Transactions> findByUserIdAndSortByDateDesc(UUID userId);

}
