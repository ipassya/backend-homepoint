package com.server.homepoint.config;

import com.server.homepoint.dto.user.JwtResponseDto;
import com.server.homepoint.filter.JwtAuthenticationFilter;
import com.server.homepoint.security.UserGoogle;
import com.server.homepoint.service.impl.CustomOAuth2UserService;
import com.server.homepoint.service.impl.JpaUserDetailsService;
import org.aspectj.weaver.patterns.IToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
public class JwtConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JpaUserDetailsService jpaUserDetailsService;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthEntryPoint;
    //
    @Autowired
    private JwtAuthenticationFilter jwtFilter;

    @Autowired
    private CustomOAuth2UserService oauthUserService;

    @Autowired
    private UserGoogle userGoogle;


    //authentication

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jpaUserDetailsService);
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("admin")
//                .roles("admin");
    }

    //authorization
    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.csrf().disable().cors().disable().authorizeRequests()
//                .antMatchers("/api/user/getall")
//                .hasRole("ADMIN").anyRequest().permitAll();

        http.csrf().disable()
                .cors().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthEntryPoint)
                .and()
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/api/v1/users/login",
                        "/api/v1/users/register",
                        "/api/v1/users/oauth/*",
                        "/api/v1/products",
                        "/api/v1/products/{id}",
                        "/api/v1/products/category/{id}",
                        "/api/v1/products/discount",
                        "/api/v1/products/latest",
                        "/api/v1/products/search",
                        "/api/v1/product-categories",
                        "/api/v1/product-categories/{id}",
                        "/api/v1/product-subcategories",
                        "/api/v1/product-subcategories/{id}",
                        "/*"
                ).permitAll()// allow this endpoint without authentication
                .anyRequest().permitAll()// for any other request, authentication should be performed
                .and()
                .oauth2Login()
                    .loginPage("/")
                    .userInfoEndpoint().userService(oauthUserService)
                    .and()
                    .successHandler(new AuthenticationSuccessHandler() {
                            @Override
                            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                                
                             DefaultOidcUser oauthUser = (DefaultOidcUser) authentication.getPrincipal();

                                String email = oauthUser.getEmail();

                                String name = oauthUser.getName();

                                try {
                                    userGoogle.processOAuthPostLogin(email, name);
                                    response.sendRedirect("/api/v1/users/oauth?email="+email);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                System.out.println("email "+oauthUser.getEmail() + "ini tidak terdaftar google");
                            }
                        })
                .and()
                .logout().logoutSuccessUrl("/").permitAll();
                // every request should be independent of other and server does not have to manage session
    }

    @Bean
    public BCryptPasswordEncoder getPasswordEncoder(){ return new BCryptPasswordEncoder();}

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception{
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

}
