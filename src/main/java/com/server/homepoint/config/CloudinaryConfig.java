package com.server.homepoint.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudinaryConfig {
    @Value("${cloudinary.cloud.name:null}")
    private String CLOUD_NAME;

    @Value("${cloudinary.api.key:null}")
    private String API_KEY;

    @Value("${cloudinary.api.secret:null}")
    private String API_SECRET;

    @Bean
    public Cloudinary cloudinary() {
        String cloudName = this.CLOUD_NAME;
        String apiKey = this.API_KEY;
        String apiSecret = this.API_SECRET;

        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret,
                "secure", true));
    }
}
