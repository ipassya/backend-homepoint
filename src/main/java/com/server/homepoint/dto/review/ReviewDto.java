package com.server.homepoint.dto.review;

import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.user.UserReadDto;
import com.server.homepoint.model.Products;
import com.server.homepoint.model.Users;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDto {
    private UUID id;
    private UserReadDto users;
    private ProductDto products;
    private Integer rating;
    private Date createAt;
}
