package com.server.homepoint.dto.review;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ReviewInsertDto {
    private UUID id;
    private UUID userId;
    private UUID transactionItemId;
    private Integer rating;
}
