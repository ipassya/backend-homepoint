package com.server.homepoint.dto.productCategory;

import com.server.homepoint.model.ProductSubcategories;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductCategoryDto {
    private UUID id;
    private Set<ProductSubcategories> productSubcategories = new LinkedHashSet<>();
    private String name;
    private String icon;
    private Boolean isDeleted;
}
