package com.server.homepoint.dto.cartItem;

import com.server.homepoint.dto.product.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartItemDto {
    private UUID id;
    private Integer quantity;
    private ProductDto products;
    private Date updatedAt;
}
