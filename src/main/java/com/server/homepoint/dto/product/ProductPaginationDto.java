package com.server.homepoint.dto.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductPaginationDto {
    private Integer totalPage;
    private Long totalRecord;
    private Integer currentPage;
    private Integer pageSize;
    private List<ProductDto> products;
    private List<String> brands;
    private List<String> colors;
}
