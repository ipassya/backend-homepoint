package com.server.homepoint.dto.product;

import com.server.homepoint.dto.productSubcategory.ProductSubcategoryDto;
import com.server.homepoint.dto.productSubcategory.ProductSubcategoryListDto;
import com.server.homepoint.model.ProductImages;
import com.server.homepoint.model.ProductSubcategories;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Setter
@Getter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {
    private UUID id;
    private ProductSubcategoryListDto productSubcategories;
    private Set<ProductImages> productImages = new LinkedHashSet<>();
    private String name;
    private String description;
    private String brand;
    private Double price;
    private Double discount;
    private Integer stock;
    private String color;
    private Double ratingAverage;
    private Integer ratingCount;
    private Integer amountSold;
    private String createdAt;
    private Boolean isDeleted;
}
