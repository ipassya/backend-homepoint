package com.server.homepoint.dto.address;

import com.server.homepoint.dto.user.UserReadDto;
import com.server.homepoint.model.Users;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddressDto {

    private UUID id;

    private UserReadDto users;

    private String label;

    private String province;

    private String city;

    private String districts;

    private String village;

    private String zipCode;

    private String fullAddress;

    private String recipientName;

    private String phoneNumber;

    private Boolean isActive;

}
