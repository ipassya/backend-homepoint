package com.server.homepoint.dto.address;

import com.server.homepoint.dto.user.UserReadDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddressUpdateDto {
    private UUID id;

    private String label;

    private String province;

    private String city;

    private String districts;

    private String village;

    private String zipCode;

    private String fullAddress;

    private String recipientName;

    private String phoneNumber;

    private Boolean isActive;

    private UUID usersId;
}
