package com.server.homepoint.dto.Transactions;

import com.server.homepoint.dto.transactionItem.TransactionItemInsertDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionsInsertDto {
    private UUID id;
    private Double totalPrice;
    private String storeLocation;
    private UUID bankId;
    private UUID shippingServicesId;
    private UUID userId;
    private UUID addressesId;
    private List<TransactionItemInsertDto> TransactionItems;
}
