package com.server.homepoint.dto.Transactions;

import com.server.homepoint.dto.address.AddressReadDto;
import com.server.homepoint.dto.transactionItem.TransactionItemDto;
import com.server.homepoint.dto.user.UserReadDto;
import com.server.homepoint.model.Addresses;
import com.server.homepoint.model.Bank;
import com.server.homepoint.model.ShippingServices;
import com.server.homepoint.model.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionReadDto {
    private UUID id;
    private Double totalPrice;
    private String status;
    private String paymentProof;
    private Date paymentDeadline;
    private String storeLocation;
    private Date createdAt;
    private UserReadDto users;
    private AddressReadDto addresses;
    private ShippingServices shippingServices;
    private Bank bank;
    private List<TransactionItemDto> TransactionItems;
}
