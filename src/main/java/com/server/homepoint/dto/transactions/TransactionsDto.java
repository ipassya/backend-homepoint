package com.server.homepoint.dto.Transactions;

import com.server.homepoint.dto.transactionItem.TransactionItemDto;
import com.server.homepoint.model.TransactionItems;
import com.server.homepoint.model.Users;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionsDto {
    private UUID id;
    private Double totalPrice;
    private String status;
    private String paymentProof;
    private Date paymentDeadline;
    private String storeLocation;
    private Date createdAt;
    private Users users;
    private List<TransactionItemDto> TransactionItems;
}
