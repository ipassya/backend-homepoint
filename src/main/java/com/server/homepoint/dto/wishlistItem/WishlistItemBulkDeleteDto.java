package com.server.homepoint.dto.wishlistItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WishlistItemBulkDeleteDto {
    private UUID[] wishlistItemIds;
}
