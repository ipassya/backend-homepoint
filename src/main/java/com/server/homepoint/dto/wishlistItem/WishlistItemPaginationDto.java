package com.server.homepoint.dto.wishlistItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class WishlistItemPaginationDto {
    private Integer totalPage;
    private Long totalRecord;
    private Integer currentPage;
    private Integer pageSize;
    private List<WishlistItemDto> wishlistItems;
}
