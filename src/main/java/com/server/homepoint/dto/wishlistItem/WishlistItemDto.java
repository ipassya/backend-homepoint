package com.server.homepoint.dto.wishlistItem;

import com.server.homepoint.dto.product.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WishlistItemDto {
    private UUID id;
    private ProductDto products;
    private Date createdAt;
}
