package com.server.homepoint.dto.transactionItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionItemInsertDto {
    private UUID id;
    private Integer price;
    private Integer quantity;
    private Integer discount;
    private Boolean isInsurance;
    private UUID productId;
}
