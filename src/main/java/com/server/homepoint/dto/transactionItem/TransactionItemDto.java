package com.server.homepoint.dto.transactionItem;

import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.review.ReviewDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionItemDto {
    private UUID id;
    private Integer price;
    private Integer quantity;
    private Boolean isInsurance;
    private ProductDto products;
    private ReviewDto reviews;
}
