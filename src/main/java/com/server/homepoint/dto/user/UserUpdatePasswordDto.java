package com.server.homepoint.dto.user;

import com.server.homepoint.dto.address.AddressReadDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdatePasswordDto {

    private UUID id;

    private List<AddressReadDto> addresses;

    private String name;

    private String phoneNumber;

    private String email;

    private String password;

    private Date joinedSince;

    private Date birthDate;

    private String gender;

    private Boolean isActive;

    private String verificationCode;

    private String resetPasswordToken;
}
