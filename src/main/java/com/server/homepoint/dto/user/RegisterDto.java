package com.server.homepoint.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.server.homepoint.util.validatoremail.UniqueEmail;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterDto {

    @NotEmpty(message = "name is Required")
    private String name;

    @UniqueEmail
    @NotEmpty(message = "Email is Required")
    @Email(message = "Email is Not Valid")
    private String email;

    @NotEmpty(message = "Password is Required")
    private String password;
}
