package com.server.homepoint.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.server.homepoint.dto.address.AddressDto;
import com.server.homepoint.dto.address.AddressReadDto;
import com.server.homepoint.model.Addresses;
import com.server.homepoint.model.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDto {

    private UUID id;

    private List<AddressReadDto> addresses;

    private String name;

    private String phoneNumber;

    private String email;

    private Date joinedSince;

    private Date birthDate;

    private String gender;

    private Boolean isActive;
}
