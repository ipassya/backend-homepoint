package com.server.homepoint.dto.user;

import com.server.homepoint.model.Addresses;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserReadDto {
    private UUID id;

    private String name;

    private String phoneNumber;

    private String email;

    private Date birthDate;

    private String gender;

    private Date joinedSince;

    private Boolean isActive;

}
