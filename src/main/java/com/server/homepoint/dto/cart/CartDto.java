package com.server.homepoint.dto.cart;

import com.server.homepoint.model.CartItems;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDto {

    private UUID id;

    private List<CartItems> cartItems;

}
