package com.server.homepoint.dto.wishlist;

import com.server.homepoint.model.WishlistItems;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WishlistDto {
    private UUID id;
    private List<WishlistItems> wishlistItems;
}
