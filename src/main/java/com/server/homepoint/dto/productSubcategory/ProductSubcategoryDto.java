package com.server.homepoint.dto.productSubcategory;

import com.server.homepoint.model.ProductCategories;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductSubcategoryDto {
    private UUID id;
    private ProductCategories productCategories;
    private String name;
    private String icon;
    private Boolean isDeleted;
}
