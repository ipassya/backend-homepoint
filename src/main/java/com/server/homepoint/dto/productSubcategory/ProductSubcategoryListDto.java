package com.server.homepoint.dto.productSubcategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ProductSubcategoryListDto {
    private UUID id;
    private String name;
    private String icon;
    private Boolean isDeleted;
}
