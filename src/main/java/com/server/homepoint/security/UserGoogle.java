package com.server.homepoint.security;

import com.server.homepoint.model.ERoles;
import com.server.homepoint.model.Provider;
import com.server.homepoint.model.Role;
import com.server.homepoint.model.Users;
import com.server.homepoint.repository.RoleRepository;
import com.server.homepoint.repository.UserRepository;
import com.server.homepoint.service.impl.JpaUserDetailsService;
import com.server.homepoint.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class UserGoogle {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    public void processOAuthPostLogin(String username, String name) throws Exception {
        Users users = userRepository.getUsersByEmail(username);

        if (users == null) {

            ERoles rolen;

            rolen = ERoles.ROLE_CUSTOMER;

            Role role = this.roleRepository.findByName(rolen).orElseThrow(()->new Exception( rolen.name()+" cannot be found in database"));

            Set<Role> roles = new HashSet<>();

            roles.add(role);

            Users newUser = new Users();
            newUser.setEmail(username);
            newUser.setName(name);
            newUser.setProvider(Provider.GOOGLE);
            newUser.setIsActive(true);
            newUser.setRoles(roles);
            newUser.setPassword(new BCryptPasswordEncoder().encode("123456"));

            System.out.println("Masuk Google");

             userRepository.save(newUser);
        } else {
            System.out.println("Akun Telah Terdaftar");
        }
    }
}
