package com.server.homepoint.controller;

import com.server.homepoint.model.Bank;
import com.server.homepoint.model.ShippingServices;
import com.server.homepoint.service.ShippingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/shippingservice")
@Api(tags = "Shipping Service")
public class ShippingServiceController {

    @Autowired
    ShippingService shippingService;

    @GetMapping
    public Iterable<ShippingServices> findAll(){
        return shippingService.getall();
    }
}
