package com.server.homepoint.controller;

import com.server.homepoint.dto.user.RoleDto;
import com.server.homepoint.service.RoleService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/roles")
@Api(tags = "Role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PostMapping()
    public ResponseEntity<RoleDto> create(@RequestBody RoleDto roleDto){
        try{
            RoleDto dto = roleService.create(roleDto);
            return new ResponseEntity<>(dto, HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
