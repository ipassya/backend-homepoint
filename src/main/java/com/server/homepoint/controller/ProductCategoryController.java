package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.productCategory.ProductCategoryDto;
import com.server.homepoint.service.ProductCategoryService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/product-categories")
@Api(tags = "Product Categories")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @PostMapping()
    public ResponseEntity<ResponseDto<ProductCategoryDto>> createProductCategory(@RequestBody ProductCategoryDto productCategoryDto) {
        try {
            ProductCategoryDto productCategory = productCategoryService.create(productCategoryDto);
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoSuccess(productCategory, "Product category created successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductCategoryDto>> updateProductCategory(@PathVariable UUID id, @RequestBody ProductCategoryDto productCategoryDto) {
        try {
            ProductCategoryDto productCategory = productCategoryService.update(id, productCategoryDto);
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoSuccess(productCategory, "Product category updated successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductCategoryDto>> deleteProductCategory(@PathVariable UUID id) {
        try {
            ProductCategoryDto productCategory = productCategoryService.delete(id);
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoSuccess(productCategory, "Product category deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductCategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping()
    public ResponseEntity<ResponseDto<List<ProductCategoryDto>>> findAllProductCategories() {
        try {
            ResponseDto<List<ProductCategoryDto>> response =
                    ResponseUtil.responseDtoSuccess(productCategoryService.findAll(), "Success retrieve all product categories");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductCategoryDto>> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductCategoryDto>> findOneProductCategory(@PathVariable UUID id) {
        try {
            ResponseDto<ProductCategoryDto> response =
                    ResponseUtil.responseDtoSuccess(productCategoryService.findById(id), "Success retrieve one product category");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductCategoryDto> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
