package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.cartItem.CartItemBulkDeleteDto;
import com.server.homepoint.dto.cartItem.CartItemDto;
import com.server.homepoint.service.CartItemService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/cart")
@Api(tags = "Cart")
public class CartController {

    @Autowired
    private CartItemService cartItemService;

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDto<List<CartItemDto>>> findCartByUserId(@PathVariable UUID userId) {
        try {
            List<CartItemDto> cartItemDto = cartItemService.getCartItems(userId);
            ResponseDto<List<CartItemDto>> response = ResponseUtil.responseDtoSuccess(cartItemDto, "Cart found successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<CartItemDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/items/{userId}/{productId}")
    public ResponseEntity<ResponseDto<CartItemDto>> findProduct(@PathVariable UUID userId, @PathVariable UUID productId) {
        try {
            CartItemDto cartItem = cartItemService.findProduct(userId, productId);
            String message = cartItem == null ? "Cart item not found" : "Cart item found successfully";
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoSuccess(cartItem, message);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/items/{userId}/{productId}")
    public ResponseEntity<ResponseDto<CartItemDto>> addCartItem(@PathVariable UUID userId, @PathVariable UUID productId, @RequestBody Integer quantity) {
        try {
            if (userId == null || productId == null) {
                return new ResponseEntity<>(ResponseUtil.responseDtoFailed(null, "Invalid request", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            } else {
                CartItemDto cartItem = cartItemService.addCartItem(userId, productId, quantity);
                ResponseDto<CartItemDto> response = ResponseUtil.responseDtoSuccess(cartItem, "Cart item added successfully");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/items/{id}")
    public ResponseEntity<ResponseDto<CartItemDto>> updateCartItem(@PathVariable UUID id, @RequestBody Integer quantity) {
        try {
            CartItemDto cartItem = cartItemService.updateCartItem(id, quantity);
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoSuccess(cartItem, "Cart item updated successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/items/{id}")
    public ResponseEntity<ResponseDto<CartItemDto>> deleteCartItem(@PathVariable UUID id) {
        try {
            CartItemDto cartItem = cartItemService.deleteCartItem(id);
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoSuccess(cartItem, "Cart item deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CartItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/items")
    public ResponseEntity<ResponseDto<List<CartItemDto>>> deleteBulkCartItems(@RequestBody CartItemBulkDeleteDto cartItemIds) {
        try {
            List<CartItemDto> cartItems = cartItemService.deleteBulkCartItems(cartItemIds);
            ResponseDto<List<CartItemDto>> response = ResponseUtil.responseDtoSuccess(cartItems, "Cart items deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<CartItemDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
