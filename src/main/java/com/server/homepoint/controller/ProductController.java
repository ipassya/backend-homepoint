package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.product.ProductDto;
import com.server.homepoint.dto.product.ProductPaginationDto;
import com.server.homepoint.service.ProductService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/products")
@Api(tags = "Products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping()
    public ResponseEntity<ResponseDto<ProductDto>> createProduct(@RequestBody ProductDto productDto) {
        try {
            ProductDto product = productService.create(productDto);
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoSuccess(product, "Product created successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductDto>> updateProduct(@PathVariable UUID id, @RequestBody ProductDto productDto) {
        try {
            ProductDto product = productService.update(id, productDto);
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoSuccess(product, "Product updated successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductDto>> deleteProduct(@PathVariable UUID id) {
        try {
            ProductDto product = productService.delete(id);
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoSuccess(product, "Product deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Get list of products in pagination form with multiple query parameters")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list of products"),
    })
    @GetMapping()
    public ResponseEntity<ResponseDto<ProductPaginationDto>> findProducts(
            @RequestParam(value = "Page number", defaultValue = "0") Integer page,
            @RequestParam(value = "Page size", defaultValue = "16") Integer size,
            @RequestParam(value = "Product name", defaultValue = "") String name,
            @RequestParam(value = "Product subcategory", defaultValue = "") UUID subcategoryId,
            @RequestParam(value = "Min price", defaultValue = "") Double priceMin,
            @RequestParam(value = "Max price", defaultValue = "") Double priceMax,
            @RequestParam(value = "Rating", defaultValue = "") Double rating,
            @RequestParam(value = "Brand", defaultValue = "") String brand,
            @RequestParam(value = "Color", defaultValue = "") String color,
            @RequestParam(value = "Description", defaultValue = "") String description,
            @RequestParam(value = "Sort by price asc", defaultValue = "") Boolean sortByPriceAsc,
            @RequestParam(value = "Sort by price desc", defaultValue = "") Boolean sortByPriceDesc,
            @RequestParam(value = "Sort by best seller", defaultValue = "") Boolean sortByBestSeller,
            @RequestParam(value = "Sort by latest", defaultValue = "") Boolean sortByLatest,
            @RequestParam(value = "Sort by discount", defaultValue = "") Boolean sortByBiggestDiscount
            ) {
        try {
            ResponseDto<ProductPaginationDto> response =
                    ResponseUtil.responseDtoSuccess(productService.findProducts(
                            page, size, name, subcategoryId, priceMin, priceMax, rating, brand, color, description, sortByPriceAsc, sortByPriceDesc, sortByBestSeller, sortByLatest, sortByBiggestDiscount),
                            "Products found successfully"
                    );
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductPaginationDto> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductDto>> findOneProduct(@PathVariable UUID id) {
        try {
            ResponseDto<ProductDto> response =
                    ResponseUtil.responseDtoSuccess(productService.findById(id), "Success retrieve product");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductDto> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/latest")
    public ResponseEntity<ResponseDto<List<ProductDto>>> findLatestProducts() {
        try {
            ResponseDto<List<ProductDto>> response =
                    ResponseUtil.responseDtoSuccess(productService.findLatestProducts(), "Success retrieve latest products");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductDto>> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/discount")
    public ResponseEntity<ResponseDto<List<ProductDto>>> findByDiscount() {
        try {
            ResponseDto<List<ProductDto>> response =
                    ResponseUtil.responseDtoSuccess(productService.findDiscountProducts(), "Success retrieve products");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductDto>> response =
                    ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
