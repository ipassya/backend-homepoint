package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.review.ReviewDto;
import com.server.homepoint.dto.review.ReviewInsertDto;
import com.server.homepoint.service.ReviewService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/reviews")
@Api(tags = "Reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @PostMapping()
    public ResponseEntity<ResponseDto<ReviewDto>> createReview(@RequestBody ReviewInsertDto reviewDto) {
        try {
            ReviewDto review = reviewService.create(reviewDto);
            ResponseDto<ReviewDto> response = ResponseUtil.responseDtoSuccess(review, "Review created successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<ReviewDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/product/{productId}")
    public ResponseEntity<ResponseDto<List<ReviewDto>>> getReviewsByProductId(@PathVariable("productId") UUID productId) {
        try {
            List<ReviewDto> review = reviewService.getByProductId(productId);
            ResponseDto<List<ReviewDto>> response = ResponseUtil.responseDtoSuccess(review, "Reviews retrieved successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ReviewDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
