package com.server.homepoint.controller;

import com.server.homepoint.dto.Transactions.TransactionsInsertDto;
import com.server.homepoint.dto.address.AddressDto;
import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.address.AddressInsertDto;
import com.server.homepoint.dto.address.AddressReadDto;
import com.server.homepoint.dto.address.AddressUpdateDto;
import com.server.homepoint.model.Addresses;
import com.server.homepoint.service.AddressService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/address")
@Api(tags = "Address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<ResponseDto<AddressDto>> create(@RequestBody AddressInsertDto addressInsertDto){
        try{
            AddressDto addressDto1 = addressService.create(addressInsertDto);

            ResponseDto<AddressDto> response = ResponseUtil.responseDtoSuccess(addressDto1, "Address Create Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<AddressDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<AddressDto>>> findAll(){
        try{
            List<AddressDto> addressDto1 = addressService.getall();

            ResponseDto<List<AddressDto>> response = ResponseUtil.responseDtoSuccess(addressDto1, "Address Get Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<AddressDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public Addresses findOne(@PathVariable("id") UUID id){
        return addressService.findOne(id);
    }

    @PutMapping
    public ResponseEntity<ResponseDto<AddressDto>> update(@RequestBody AddressUpdateDto addressUpdateDto){
        try{
            AddressDto addressDto1 = addressService.update(addressUpdateDto);

            ResponseDto<AddressDto> response = ResponseUtil.responseDtoSuccess(addressDto1, "Update Create Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<AddressDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public void removeOne(@PathVariable("id") UUID id){
        addressService.removeOne(id);
    }

}
