package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.Transactions.TransactionReadDto;
import com.server.homepoint.dto.Transactions.TransactionsInsertDto;

import com.server.homepoint.service.TransactionService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/transaction")
@Api(tags = "Transaction")
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @PostMapping()
    public ResponseEntity<ResponseDto<TransactionReadDto>> createTransaction(@RequestBody TransactionsInsertDto transactionsInsertDto){
        try{
            TransactionReadDto transactions = transactionService.create(transactionsInsertDto);

            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoSuccess(transactions, "Transaction Create Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{transactionId}")
    public ResponseEntity<ResponseDto<TransactionReadDto>> getTransactionById(@PathVariable("transactionId") UUID transactionId){
        try{
            TransactionReadDto transactions = transactionService.findByTransactionId(transactionId);

            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoSuccess(transactions, "Transaction Get Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<ResponseDto<List<TransactionReadDto>>> getTransactionByUserId(@PathVariable UUID userId){
        try{
            List<TransactionReadDto> transactions = transactionService.findByUserId(userId);

            ResponseDto<List<TransactionReadDto>> response = ResponseUtil.responseDtoSuccess(transactions, "Transaction Get Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<TransactionReadDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/payment-confirmation/{transactionId}")
    public ResponseEntity<ResponseDto<TransactionReadDto>> uploadPaymentProof(@PathVariable UUID transactionId, MultipartFile paymentProof){
        try{
            TransactionReadDto transactions = transactionService.uploadPaymentProof(transactionId, paymentProof);

            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoSuccess(transactions, "Upload Payment Proof Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/status/{transactionId}")
    public ResponseEntity<ResponseDto<TransactionReadDto>> updateStatus(@PathVariable UUID transactionId, @RequestBody String status){
        try{
            TransactionReadDto transactions = transactionService.updateStatus(transactionId, status);

            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoSuccess(transactions, "Update Status Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<TransactionReadDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
