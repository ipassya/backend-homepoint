package com.server.homepoint.controller;

import com.server.homepoint.model.Addresses;
import com.server.homepoint.model.Bank;
import com.server.homepoint.service.BankService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/bank")
@Api(tags = "Bank")
public class BankController {

    @Autowired
    BankService bankService;

    @GetMapping
    public Iterable<Bank> findAll(){
        return bankService.getall();
    }

}
