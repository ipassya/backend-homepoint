package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemBulkDeleteDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemDto;
import com.server.homepoint.dto.wishlistItem.WishlistItemPaginationDto;
import com.server.homepoint.service.WishlistItemService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/wishlist")
@Api(tags = "Wishlist")
public class WishlistController {

    @Autowired
    private WishlistItemService wishlistItemService;

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDto<WishlistItemPaginationDto>> findWishlistByUserId(
            @PathVariable UUID userId,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "12") Integer size,
            @RequestParam(defaultValue = "") String productName
    ) {
        try {
            ResponseDto<WishlistItemPaginationDto> response = ResponseUtil.responseDtoSuccess(
                    wishlistItemService.getWishlistItems(userId, page, size, productName),
                    "Wishlist found successfully"
            );
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<WishlistItemPaginationDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/items/{userId}/{productId}")
    public ResponseEntity<ResponseDto<WishlistItemDto>> findProduct(@PathVariable UUID userId, @PathVariable UUID productId) {
        try {
            WishlistItemDto wishlistItem = wishlistItemService.findProduct(userId, productId);
            String message = wishlistItem == null ? "Wishlist item not found" : "Wishlist item found successfully";
            ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoSuccess(wishlistItem, message);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/items/{userId}/{productId}")
    public ResponseEntity<ResponseDto<WishlistItemDto>> addWishlistItem(@PathVariable UUID userId, @PathVariable UUID productId) {
        try {
            if (userId == null || productId == null) {
                return new ResponseEntity<>(ResponseUtil.responseDtoFailed(null, "Invalid request", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
            } else {
                WishlistItemDto wishlistItem = wishlistItemService.addWishlistItem(userId, productId);
                ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoSuccess(wishlistItem, "Wishlist item added successfully");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch (Exception e) {
            ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/items/{id}")
    public ResponseEntity<ResponseDto<WishlistItemDto>> deleteWishlistItem(@PathVariable UUID id) {
        try {
            WishlistItemDto wishlistItem = wishlistItemService.deleteWishlistItem(id);
            ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoSuccess(wishlistItem, "Wishlist item deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<WishlistItemDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/items")
    public ResponseEntity<ResponseDto<List<WishlistItemDto>>> deleteBulkWishlistItems(@RequestBody WishlistItemBulkDeleteDto wishlistItemIds) {
        try {
            List<WishlistItemDto> wishlistItems = wishlistItemService.deleteBulkWishlistItems(wishlistItemIds);
            ResponseDto<List<WishlistItemDto>> response = ResponseUtil.responseDtoSuccess(wishlistItems, "Wishlist items deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<WishlistItemDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(),
                    HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

}
