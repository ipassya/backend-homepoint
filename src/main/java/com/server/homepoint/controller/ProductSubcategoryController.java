package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.productSubcategory.ProductSubcategoryDto;
import com.server.homepoint.dto.productSubcategory.ProductSubcategoryListDto;
import com.server.homepoint.service.ProductSubcategoryService;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/product-subcategories")
@Api(tags = "Product SubCategories")
public class ProductSubcategoryController {

    @Autowired
    private ProductSubcategoryService productSubcategoryService;

    @PostMapping()
    public ResponseEntity<ResponseDto<ProductSubcategoryDto>> createProductSubcategory(@RequestBody ProductSubcategoryDto productSubcategoryDto) {
        try {
            ProductSubcategoryDto productSubcategory = productSubcategoryService.create(productSubcategoryDto);
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoSuccess(productSubcategory, "Product subcategory created successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductSubcategoryDto>> updateProductSubcategory(@PathVariable UUID id, @RequestBody ProductSubcategoryDto productSubcategoryDto) {
        try {
            ProductSubcategoryDto productSubcategory = productSubcategoryService.update(id, productSubcategoryDto);
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoSuccess(productSubcategory, "Product subcategory updated successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductSubcategoryDto>> deleteProductSubcategory(@PathVariable UUID id) {
        try {
            ProductSubcategoryDto productSubcategory = productSubcategoryService.delete(id);
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoSuccess(productSubcategory, "Product subcategory deleted successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping()
    public ResponseEntity<ResponseDto<List<ProductSubcategoryListDto>>> findAllProductSubcategories() {
        try {
            List<ProductSubcategoryListDto> productSubcategories = productSubcategoryService.findAll();
            ResponseDto<List<ProductSubcategoryListDto>> response = ResponseUtil.responseDtoSuccess(productSubcategories, "Product subcategories retrieved successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductSubcategoryListDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductSubcategoryDto>> findProductSubcategoryById(@PathVariable UUID id) {
        try {
            ProductSubcategoryDto productSubcategory = productSubcategoryService.findById(id);
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoSuccess(productSubcategory, "Product subcategory retrieved successfully");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductSubcategoryDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
