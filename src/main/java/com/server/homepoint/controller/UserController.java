package com.server.homepoint.controller;

import com.server.homepoint.dto.ResponseDto;
import com.server.homepoint.dto.user.*;
import com.server.homepoint.model.Users;
import com.server.homepoint.service.impl.JpaUserDetailsService;
import com.server.homepoint.service.UserService;
import com.server.homepoint.util.JwtUtil;
import com.server.homepoint.util.ResponseUtil;
import io.swagger.annotations.Api;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/users")
@Api(tags = "User")
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JpaUserDetailsService jpaUserDetailsService;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private JavaMailSender mailSender;

    @GetMapping("/oauth")
    public ResponseEntity<ResponseDto<JwtResponseDto>> oauth(@RequestParam String email) throws Exception {
        UserDto getUser = this.userService.getByEmail(email);

        ResponseDto<JwtResponseDto> responseDto = new ResponseDto<>();

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(getUser.getEmail(), 123456));

        UserDetails userDetails = jpaUserDetailsService.loadUserByUsername(getUser.getEmail());

        String jwtToken = jwtUtil.generateToken(userDetails);

        JwtResponseDto jwtResponse = new JwtResponseDto(jwtToken, getUser.getName(), getUser.getId());

        System.out.println("Masuk Sini");

        responseDto.setSuccess(true);
        responseDto.setStatus("200");
        responseDto.setMessage("Token for Login");
        responseDto.setData(jwtResponse);

        return ResponseEntity.ok(responseDto);
    }

    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ResponseEntity<ResponseDto<JwtResponseDto>> login(@ModelAttribute LoginDto loginDto) throws Exception{

        UserDto getUser = this.userService.getByEmail(loginDto.getEmail());

        ResponseDto<JwtResponseDto> responseDto = new ResponseDto<>();

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));

        UserDetails userDetails = jpaUserDetailsService.loadUserByUsername(loginDto.getEmail());

        String jwtToken = jwtUtil.generateToken(userDetails);

        JwtResponseDto jwtResponse = new JwtResponseDto(jwtToken, getUser.getName(), getUser.getId());

        responseDto.setSuccess(true);
        responseDto.setStatus("200");
        responseDto.setMessage("Token for Login");
        responseDto.setData(jwtResponse);

        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseDto<UserDto>> create(@Valid @RequestBody RegisterDto registerDto) throws Exception {
        try{
            UserDto users = userService.create(registerDto);

            ResponseDto<UserDto> response = ResponseUtil.responseDtoSuccess(users, "Register Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<UserDto>>> getAll(){
        try{
            List<UserDto> users = userService.getAll();

            ResponseDto<List<UserDto>> response = ResponseUtil.responseDtoSuccess(users, "Get User All Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<UserDto>> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/search")
    public  ResponseEntity<ResponseDto<UserDto>> getByEmail(@RequestParam String email) throws Exception {
        try{
            UserDto users = userService.getByEmail(email);

            ResponseDto<UserDto> response = ResponseUtil.responseDtoSuccess(users, "Get User Email Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ResponseDto<UserDto>> getById(@PathVariable("id") UUID id) throws  Exception{
        try{
            UserDto users = userService.getById(id);

            ResponseDto<UserDto> response = ResponseUtil.responseDtoSuccess(users, "Get User Id Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<ResponseDto<UserDto>> update(@RequestBody UserUpdateDto userDto) throws Exception{
        try{
            UserDto users = userService.update(userDto);

            ResponseDto<UserDto> response = ResponseUtil.responseDtoSuccess(users, "Update Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/password")
    public ResponseEntity<ResponseDto<UserDto>> updatePassword(@RequestBody UpdatePasswordDto userDto) throws Exception{
        try{
            UserDto users = userService.updatePassword(userDto);

            ResponseDto<UserDto> response = ResponseUtil.responseDtoSuccess(users, "Update Successfull");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public void remove(@PathVariable("id") UUID id) throws Exception {
        userService.remove(id);
    }

    @GetMapping("/verify")
    public ResponseEntity<ResponseDto<String>> verifyAccount(@Param("code") String code){
        try {
            ResponseDto<String> responseDto = new ResponseDto<>();

            boolean verified = userService.verify(code);

            responseDto.setSuccess(true);
            responseDto.setStatus("200");
            responseDto.setMessage("Success For Update Users");

            responseDto.setData("Account has been activated");

            return ResponseEntity.status(HttpStatus.OK).body(responseDto);
        }catch (Exception e){
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/forgot_password")
    public ResponseEntity<ResponseDto<String>> processForgotPassword(HttpServletRequest request, String email) throws Exception {
        try {
            String token = RandomString.make(45);

            userService.updateResetPassword(token, email);

            String scheme = request.getScheme();
            String host = request.getHeader("Host");
            String siteURL = scheme + "://"+ host +"/api/v1/users";

            String resetPasswordLink = siteURL + "/reset_password?token=" + token;

            userService.sendEmail(email, resetPasswordLink);

            ResponseDto<String> response = ResponseUtil.responseDtoSuccess(email, "Sent to Email Success");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<String> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/reset_password")
    public ResponseEntity<ResponseDto<String>> showResetPassword(@Param(value = "token") String token){
        try {
            Users users = userService.getByResetPasswordToken(token);

            ResponseDto<String> response = ResponseUtil.responseDtoSuccess(token, "Masukkan Token ini ke form hidden token");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<String> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/reset_password")
    public ResponseEntity<ResponseDto<UserUpdatePasswordDto>> processResetPassword(@RequestBody ForgotPasswordDto forgotPasswordDto) throws Exception {
        try {

            //        String token = request.getParameter("token");
//        String password = request.getParameter("password");

            Users users = userService.getByResetPasswordToken(forgotPasswordDto.getToken());

            userService.updatePassword(users, forgotPasswordDto.getPassword());


            ResponseDto<UserUpdatePasswordDto> response = ResponseUtil.responseDtoSuccess(null, "Password Berhasil Diganti");

            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<UserUpdatePasswordDto> response = ResponseUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.CREATED);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    };


}
